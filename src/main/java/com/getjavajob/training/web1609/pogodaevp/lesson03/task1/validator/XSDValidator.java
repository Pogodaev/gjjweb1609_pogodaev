package com.getjavajob.training.web1609.pogodaevp.lesson03.task1.validator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * Created by paul on 07.10.16.
 */
public class XSDValidator {
    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        System.out.println(check("data.xml", "forData.xsd"));
    }

    private static boolean check(String pathXml, String pathXsd) throws ParserConfigurationException, IOException, SAXException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = parser.parse(new File(classLoader.getResource(pathXml).getFile()));

        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        Source schemaFile = new StreamSource(new File(classLoader.getResource(pathXsd).getFile()));
        Schema schema = factory.newSchema(schemaFile);
        Validator validator = schema.newValidator();

        try {
            validator.validate(new DOMSource(document));
            return true;
        } catch (SAXException e) {
            System.out.println("instance document is invalid!");
            return false;
        }
    }
}
