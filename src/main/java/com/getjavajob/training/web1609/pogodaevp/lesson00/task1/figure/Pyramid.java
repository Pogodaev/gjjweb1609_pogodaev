package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.figure;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis.CornerBasis;

/**
 * Created by paul on 13.09.16.
 */
public class Pyramid extends ConeLike implements Figure {

    public Pyramid(CornerBasis cornerBasis, double height) {
        super(cornerBasis, height);
    }

    public double getHeight() {
        return super.getHeight();
    }

    public void setHeight(double height) {
        super.setHeight(height);
    }

    public double getBorder() {
        return super.getBasis().returnSide();
    }

    @Override
    public double countSurfaceArea() {
        double h = getHeight();
        double r = (2 * super.getBasis().countBasisArea()) / (getBasis().returnCorners() * getBorder());
        double apofema = Math.sqrt(h * h + r * r);
        return getBasis().countBasisArea() + 0.5 * getBorder() * getBasis().returnCorners() * apofema;
    }
}
