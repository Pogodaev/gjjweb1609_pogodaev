package com.getjavajob.training.web1609.pogodaevp.lesson02;

import java.io.Serializable;
import java.util.List;

/**
 * Created by paul on 22.10.16.
 */
public class Datas implements Serializable {
    private List<Data> dataList;

    public Datas() {
    }

    public Datas(List<Data> dataList) {
        this.dataList = dataList;
    }

    public List<Data> getDataList() {
        return dataList;
    }

    public void setDataList(List<Data> dataList) {
        this.dataList = dataList;
    }
}
