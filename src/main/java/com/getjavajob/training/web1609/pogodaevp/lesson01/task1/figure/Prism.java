package com.getjavajob.training.web1609.pogodaevp.lesson01.task1.figure;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task1.basis.PrismBasis;

/**
 * Created by paul on 13.09.16.
 */
public class Prism implements Figure, Cloneable {
    private int corners;
    private double height;
    private double border;
    public PrismBasis prismBasis;

    @Override
    protected Prism clone() {
        try {
            Prism copy = (Prism) super.clone();
            copy.prismBasis = copy.prismBasis.clone();
            return copy;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return "Prism{" +
                "corners=" + corners +
                ", height=" + height +
                ", border=" + border +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Prism prism = (Prism) o;

        if (getCorners() != prism.getCorners()) return false;
        if (Double.compare(prism.getHeight(), getHeight()) != 0) return false;
        return Double.compare(prism.getBorder(), getBorder()) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getCorners();
        temp = Double.doubleToLongBits(getHeight());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getBorder());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public Prism(PrismBasis prismBasis) {
        this.prismBasis = prismBasis;
        this.corners = prismBasis.getCorners();
        this.height = prismBasis.getHeight();
        this.border = prismBasis.getBorder();
    }

    public Prism(int corners, double height, double border) {
        this.corners = corners;
        this.height = height;
        this.border = border;
    }

    public int getCorners() {
        return corners;
    }

    public void setCorners(int corners) {
        this.corners = corners;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getBorder() {
        return border;
    }

    public void setBorder(double border) {
        this.border = border;
    }

    @Override
    public double countSurfaceArea() {
        return getCorners() * getHeight() * getBorder() + 2 * prismBasis.countBasisArea();
    }

    @Override
    public double countVolume() {
        return prismBasis.countBasisArea() * getHeight();
    }
}
