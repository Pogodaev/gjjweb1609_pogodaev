package com.getjavajob.training.web1609.pogodaevp.lesson07.task2;

import com.getjavajob.training.web1609.pogodaevp.lesson10.ForkJoinSort;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;


/**
 * Created by paul on 10.10.16.
 */
public class Measure {
    final static int MAX_VAL = 500000;

    public static void main(String[] args) {
        Random random = new Random();
        Thing[] t = new Thing[MAX_VAL];
        Thing[] cp = new Thing[MAX_VAL];
        Thing[] cmp = new Thing[MAX_VAL];

        for (int i = 0; i < MAX_VAL; i++) {
            String tmp = String.valueOf(random.nextInt());
            Thing to = new Thing(tmp);
            t[i] = to;
            cp[i] = to;
            cmp[i] = to;
        }

        SingleSort sSort = new SingleSort();
        MultiSort multiSort = new MultiSort();

        long before = new Date().getTime();
        sSort.sort(cp);
        System.out.println("Time = " + (new Date().getTime() - before));

        before = new Date().getTime();
        multiSort.quicksort(t, 0, t.length - 1, 0);
        System.out.println("Time = " + (new Date().getTime() - before));

        ForkJoinPool pool = new ForkJoinPool();
        ForkJoinSort forkJoinSort = new ForkJoinSort<Thing>(cmp);

        before = new Date().getTime();
        pool.invoke(forkJoinSort);
        System.out.println("Time = " + (new Date().getTime() - before));

        for (int i = 0; i < MAX_VAL; i++) {
            if (cmp[i].compareTo(t[i]) != 0) {
                System.out.println(i + " " + cmp[i] + " bad " + t[i]);
            }
        }
    }
}
