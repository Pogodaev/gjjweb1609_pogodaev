package com.getjavajob.training.web1609.pogodaevp.lesson03.task2;

import com.getjavajob.training.web1609.pogodaevp.lesson03.task2.annotations.XMLAlias;
import com.getjavajob.training.web1609.pogodaevp.lesson03.task2.annotations.XMLAttribute;
import com.getjavajob.training.web1609.pogodaevp.lesson03.task2.annotations.XMLTransient;

/**
 * Created by paul on 07.10.16.
 */
@XMLAlias(value = "Sample")
public class Sample {

    @XMLAlias(value = "thisName")
    private String name;
    @XMLAttribute("id")
    @XMLAlias(value = "age")
    private int age;
    @XMLAlias(value = "Huma-huma")
    @XMLTransient
    private Human myHuman;

    public Sample(String name, int age, Human human) {
        this.name = name;
        this.age = age;
        this.myHuman = human;
    }

    public Sample() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Human getMyHuman() {
        return myHuman;
    }

    public void setMyHuman(Human myHuman) {
        this.myHuman = myHuman;
    }
}
