package com.getjavajob.training.web1609.pogodaevp.lesson03.task1;

import java.io.Serializable;

/**
 * Created by paul on 19.09.16.
 */
public final class Organization implements Serializable {
    private String orgName;

    public Organization() {
    }

    public Organization(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
}
