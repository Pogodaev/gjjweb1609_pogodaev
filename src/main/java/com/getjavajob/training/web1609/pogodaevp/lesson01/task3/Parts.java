package com.getjavajob.training.web1609.pogodaevp.lesson01.task3;

/**
 * Created by paul on 18.09.16.
 */
public enum Parts {
    AMERICA("America", 42549000, 953700000, 35),
    EUROPE("Europe", 101800000, 740000000, 50),
    ASIA("Asia", 44579000, 4164252000l, 54),
    AFRICA("Africa", 30221532, 1100000000, 55),
    AUSTRALIA("Australia", 8520000, 24200000, 14),
    ANTARCTICA("Antarctica", 14107000, 0, 0);

    private String name;
    private int area;
    private long population;
    private int countriesNumber;

    public String getName() {
        return name;
    }

    public int getArea() {
        return area;
    }

    public long getPopulation() {
        return population;
    }

    public int getCountriesNumber() {
        return countriesNumber;
    }

    Parts(String name, int area, long population, int countriesNumber) {
        this.name = name;
        this.area = area;
        this.population = population;
        this.countriesNumber = countriesNumber;
    }

    //Let it be for searching by not UPPERCASE strings
    public static Parts findByName(String string) {
        for (Parts p : values()) {
            if (p.getName().equals(string)) {
                return p;
            }
        }
        return null;
    }
}
