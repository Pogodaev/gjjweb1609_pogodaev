package com.getjavajob.training.web1609.pogodaevp.lesson07.task1;

import java.io.*;
import java.util.*;

/**
 * Created by paul on 10.10.16.
 */
public class LetterCounter {
    final static char[] alphabet = {'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'};
    final static int UP = alphabet.length;
    private static volatile HashMap<Character, Integer> map;
    private static String text;

    public static void main(String[] args) throws InterruptedException {
        LetterCounter lc = new LetterCounter();
        SortedSet<Map.Entry<Character, Integer>> set = lc.countChars(new File("Text.txt"));
        System.out.println(text.toLowerCase());
        for (Map.Entry<Character, Integer> m : set) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
    }

    public static SortedSet<Map.Entry<Character, Integer>> countChars(File file) throws InterruptedException {
        map = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        Collection<Thread> threads = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "windows-1251"))) {
            while (br.ready()) {
                sb.append(br.readLine());
            }
            text = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < UP; i++) {
            Thread thread = new Counter(alphabet[i]);
            threads.add(thread);
            thread.start();
        }

        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        SortedSet<Map.Entry<Character, Integer>> set = new TreeSet<>((o1, o2) -> {
            int res = o2.getValue().compareTo(o1.getValue());
            return res != 0 ? res : 1;
        });
        set.addAll(map.entrySet());
        return set;
    }

    private static class Counter extends Thread {
        private char letter;
        private int counter;

        public Counter(char ch) {
            this.letter = ch;
        }

        @Override
        public void run() {
            for (Character c : text.toLowerCase().toCharArray()) {
                if (letter == c) {
                    counter++;
                }
            }
            if (counter > 0) {
                synchronized (map) {
                    map.put(letter, counter);
                }
            }
        }
    }
}



