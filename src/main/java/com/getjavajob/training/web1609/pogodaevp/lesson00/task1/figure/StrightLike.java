package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.figure;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis.AbstractBasis;

/**
 * Created by paul on 23.10.16.
 */
public abstract class StrightLike extends AbstractFigure {
    public StrightLike(AbstractBasis basis, double height) {
        super(basis, height);
    }

    @Override
    public double countVolume() {
        return super.getBasis().countBasisArea() * getHeight();
    }

    @Override
    public double countCrossSection(int h) {
        return countVolume();
    }
}
