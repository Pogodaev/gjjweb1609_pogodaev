package com.getjavajob.training.web1609.pogodaevp.lesson03.task1.readers;

import com.getjavajob.training.web1609.pogodaevp.lesson02.Human;
import com.getjavajob.training.web1609.pogodaevp.lesson02.OrgType;
import com.getjavajob.training.web1609.pogodaevp.lesson02.Organization;
import com.getjavajob.training.web1609.pogodaevp.lesson02.Shareholder;
import com.getjavajob.training.web1609.pogodaevp.lesson03.task1.Data;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul on 05.10.16.
 */
public class XMLSTAXReader implements XMLParser {

    @Override
    public void parse(String file) throws ParserConfigurationException, SAXException, IOException, XMLStreamException {
        List<Data> list = new ArrayList<>();
        Data data = null;
        Human human = new Human();
        Organization org = null;
        double d;
        String name = "";
        Shareholder shareholder = new Shareholder();
        ArrayList<Shareholder> arrayList = new ArrayList<>();

        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = factory.createXMLStreamReader(ClassLoader.getSystemResourceAsStream(file));
        l1:
        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "shareholder":
                            break l1;
                        case "organization":
                            org = new Organization();
                            break;
                        case "orgName":
                            org.setOrgName(reader.getElementText());
                            data = new Data();
                            data.setOrganization(org);
                            break;
                        case "orgType":
                            data.setOrgType(OrgType.findByName(reader.getElementText().toLowerCase()));
                            break;
                        case "name":
                            name = reader.getElementText();
                            human.setName(name);
                            break;
                        case "lastname":
                            human.setSurname(reader.getElementText());
                            break;
                        case "surname":
                            human.setLastname(reader.getElementText());
                            data.setSeo(human);
                            break;
                        case "capital":
                            data.setCapital(Double.parseDouble(reader.getElementText()));
                            break;
                        case "parent":
                            data.setParentOrg(reader.getElementText());
                            arrayList.add(shareholder);
                            break;
                        case "percent":
                            d = Double.parseDouble(reader.getElementText());
                            shareholder.setPercent(d);
                            shareholder.setHuman(new Human(name, "", ""));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if ("data".equals(reader.getLocalName())) {
                        list.add(data);
                    }
                    break;
            }
        }
        System.out.println(data);
    }
}
