package com.getjavajob.training.web1609.pogodaevp.lesson02.IOFilesClass;

import com.getjavajob.training.web1609.pogodaevp.lesson02.Datas;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/**
 * Created by paul on 26.09.16.
 */
public class LoadExit {
    private Datas datas;

    public LoadExit() {
    }

    public Datas getDatas() {
        return datas;
    }

    public void setDatas(Datas datas) {
        this.datas = datas;
    }

    public void loadToFileBeforeExit(Datas d) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("oOrganizations.dat", true)))) {
            oos.writeObject(d);
        } catch (Exception e) {
            System.out.println("Catch Error");
            e.printStackTrace();
        }
    }
}
