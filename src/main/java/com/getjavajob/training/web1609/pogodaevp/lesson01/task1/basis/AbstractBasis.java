package com.getjavajob.training.web1609.pogodaevp.lesson01.task1.basis;

/**
 * Created by paul on 13.09.16.
 */
public abstract class AbstractBasis implements Basis {
    double countCircleArea(double radius) {
        return Math.PI * radius * radius;
    }

    double countCornerArea(int corners, double border) {
        return (corners * border * border) / (4 * Math.tan(Math.PI / corners));
    }
}
