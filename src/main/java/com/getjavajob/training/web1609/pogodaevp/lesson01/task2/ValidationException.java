package com.getjavajob.training.web1609.pogodaevp.lesson01.task2;

/**
 * Created by paul on 17.09.16.
 */
public class ValidationException extends Throwable {
    private String message;

    public ValidationException(String message) {
        this.message = message;
        System.out.println(message);
    }

    @Override
    public String getMessage() {
        return message;
    }
}
