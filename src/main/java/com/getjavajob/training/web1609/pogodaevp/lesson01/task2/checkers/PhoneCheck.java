package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by paul on 16.09.16.
 */
public class PhoneCheck implements Check {
    @Override
    public boolean check(Product product) throws ValidationException {
        String number = product.getPhone();
        Pattern pattern = Pattern.compile("\\+79\\d{7}");
        Matcher matcher = pattern.matcher(number);
        if (matcher.matches()) {
            return true;
        } else {
            throw new ValidationException("Проверка номера телефона не удалась: " + number);
        }
    }
}
