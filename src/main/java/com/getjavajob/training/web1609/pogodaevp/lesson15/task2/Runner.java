package com.getjavajob.training.web1609.pogodaevp.lesson15.task2;

/**
 * Created by paul on 05.01.17.
 */
public class Runner {
    public static void main(String[] args) {
        Fork f1 = new Fork();
        Fork f2 = new Fork();
        Fork f3 = new Fork();
        Fork f4 = new Fork();
        Fork f5 = new Fork();

        new Thread(new Philosoph(f5, f1)).start();
        new Thread(new Philosoph(f1, f2)).start();
        new Thread(new Philosoph(f3, f2)).start();
        new Thread(new Philosoph(f3, f4)).start();
        new Thread(new Philosoph(f5, f4)).start();
    }
}
