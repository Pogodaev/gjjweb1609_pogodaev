package com.getjavajob.training.web1609.pogodaevp.lesson08;


/**
 * Created by paul on 22.10.16.
 */
public class Runner implements Runnable {
    private int row;
    private int col;
    private int a[][];
    private int b[][];
    private int c[][];

    public Runner(int row, int col, int A[][], int B[][], int C[][]) {
        this.row = row;
        this.col = col;
        this.a = A;
        this.b = B;
        this.c = C;
        run();
    }

    @Override
    public void run() {
        try {
            ParentMatrix.mult(row, col, a, b, c);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
