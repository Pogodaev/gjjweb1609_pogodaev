package com.getjavajob.training.web1609.pogodaevp.lesson01.task1.basis;

/**
 * Created by paul on 13.09.16.
 */
public class CylinderBasis extends AbstractBasis implements Cloneable {
    private double radius;
    private double height;

    @Override
    public CylinderBasis clone() {
        try {
            return (CylinderBasis) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public CylinderBasis(double radius, double height) {
        this.radius = radius;
        this.height = height;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double countBasisArea() {
        return countCircleArea(getRadius());
    }

    @Override
    public double countCrossSection(double crossHeight) {
        return countBasisArea();
    }
}
