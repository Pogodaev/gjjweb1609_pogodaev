package com.getjavajob.training.web1609.pogodaevp.lesson07.task2;

/**
 * Created by paul on 26.10.16.
 */
public class BasicSort<T extends Comparable<T>> {
    public static <T extends Comparable<T>> int partition(T[] a, int left, int right) {
        int i = left - 1;
        int j = right;

        while (true) {
            while (a[++i].compareTo(a[right]) < 0) ;
            while (a[right].compareTo(a[--j]) < 0) {
                if (j == left) {
                    break;
                }
            }
            if (i >= j) {
                break;
            }
            exchange(a, i, j);
        }
        exchange(a, i, right);
        return i;
    }

    public static <T extends Comparable<T>> void exchange(T[] a, int i, int j) {
        T swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }
}
