package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis;

/**
 * Created by paul on 13.09.16.
 */
public abstract class AbstractBasis implements Basis {
    public AbstractBasis() {
    }

    @Override
    public int returnCorners() {
        return 0;
    }
}
