package com.getjavajob.training.web1609.pogodaevp.lesson01.task4;

import java.util.Date;

/**
 * Created by paul on 14.09.16.
 */
public class Product {
    @SimpleStringAnnotation(name = "Имя Продукта")
    private String pName;
    @SimpleStringAnnotation(name = "Заказываемое количество")
    private int needQuantity;
    @SimpleStringAnnotation(name = "Количество на складе")
    private int stockQuantity;
    @SimpleStringAnnotation(name = "Цена продукта")
    private int price;
    @SimpleStringAnnotation(name = "Скидка на продукт")
    private int discount;
    @SimpleStringAnnotation(name = "Id клиента")
    private String clientId;
    @SimpleStringAnnotation(name = "Email адресс клиента")
    private String email;
    @SimpleStringAnnotation(name = "Номер клиента")
    private String phone;
    @SimpleStringAnnotation(name = "Дата доставки")
    private Date date;

    public Product() {
    }

    public Product(String pName, int needQuantity, int stockQuantity, int price, int discount, String clientId, String email, String phone, Date date) {
        this.pName = pName;
        this.needQuantity = needQuantity;
        this.stockQuantity = stockQuantity;
        this.price = price;
        this.discount = discount;
        this.clientId = clientId;
        this.email = email;
        this.phone = phone;
        this.date = date;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public void setNeedQuantity(int needQuantity) {
        this.needQuantity = needQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getpName() {
        return pName;
    }

    public int getneedQuantity() {
        return needQuantity;
    }

    public int getstockQuantity() {
        return stockQuantity;
    }

    public int getprice() {
        return price;
    }

    public int getdiscount() {
        return discount;
    }

    public String getclientId() {
        return clientId;
    }

    public String getemail() {
        return email;
    }

    public String getphone() {
        return phone;
    }

    public Date getdate() {
        return date;
    }
}
