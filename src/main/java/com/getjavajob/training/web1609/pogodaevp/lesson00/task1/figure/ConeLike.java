package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.figure;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis.AbstractBasis;

/**
 * Created by paul on 23.10.16.
 */
public abstract class ConeLike extends AbstractFigure {

    public ConeLike(AbstractBasis basis, double height) {
        super(basis, height);
    }

    @Override
    public double countSurfaceArea() {
        return 0;
    }

    @Override
    public double countCrossSection(int h) {
        double d = getHeight();
        return (super.getBasis().countBasisArea() * h * h) / (d * d);
    }

    @Override
    public double countVolume() {
        return (1.0 / 3.0) * super.getBasis().countBasisArea() * getHeight();
    }
}
