package com.getjavajob.training.web1609.pogodaevp.lesson01.task2;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.Check;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul on 15.09.16.
 */
public class Validator {
    private List<Check> checkList;

    public Validator(ArrayList<Check> checkList) {
        this.checkList = checkList;
    }

    public void validate(Product product) {
        for (Check ch : checkList) {
            try {
                ch.check(product);
            } catch (ValidationException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
