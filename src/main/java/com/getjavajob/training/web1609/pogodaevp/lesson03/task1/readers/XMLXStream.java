package com.getjavajob.training.web1609.pogodaevp.lesson03.task1.readers;

import com.getjavajob.training.web1609.pogodaevp.lesson02.Human;
import com.getjavajob.training.web1609.pogodaevp.lesson02.OrgType;
import com.getjavajob.training.web1609.pogodaevp.lesson02.Organization;
import com.getjavajob.training.web1609.pogodaevp.lesson02.Shareholder;
import com.getjavajob.training.web1609.pogodaevp.lesson03.task1.Data;
import com.getjavajob.training.web1609.pogodaevp.lesson03.task1.Datas;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by paul on 07.10.16.
 */
public class XMLXStream implements XMLParser {
    static ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

    @Override
    public void parse(String file) throws ParserConfigurationException, SAXException, IOException, XMLStreamException {
        BufferedReader br = new BufferedReader(new FileReader(new File(classLoader.getResource("dx.xml").getFile())));
        String line;
        StringBuilder sb = new StringBuilder();

        while ((line = br.readLine()) != null) {
            sb.append(line.trim());
        }
        String res = sb.toString();
        res = res.trim().replaceFirst("^([\\W]+)<", "<");
        System.out.println(res);

        XStream stream = new XStream();
        stream.processAnnotations(Datas.class);
        Data msg = new Data();
        msg.setOrganization(new Organization("Hello"));
        msg.setSeo(new Human("Ivanov", "Ivan", "Ivanich"));
        msg.setParentOrg("NONE");
        msg.setOrgType(OrgType.findByName("ooo"));
        msg.setCapital(1234.);
        ArrayList<Shareholder> list = new ArrayList<>();
        list.add(new Shareholder(0.5, new Human("Ivan", "", "")));
        list.add(new Shareholder(0.3, new Human("Alex", "", "")));
        msg.setList(list);
        ArrayList<Data> list1 = new ArrayList<>();
        list1.add(msg);

        ArrayList<Data> lst = new ArrayList<>();
        lst.add(msg);
        System.out.println(stream.toXML(list1));

        Datas dts = new Datas(list1);

        stream.alias("datas", Datas.class);
        stream.alias("data", Data.class);
        stream.addImplicitCollection(Datas.class, "list");


        XStream xStream = new XStream(new DomDriver());
        xStream.processAnnotations(new Class[]{Datas.class, Data.class, Shareholder.class, Human.class});
        xStream.alias("datas", Datas.class);
        xStream.alias("data", Data.class);
        Datas data = (Datas) xStream.fromXML(res);
        System.out.println(data);
    }
}
