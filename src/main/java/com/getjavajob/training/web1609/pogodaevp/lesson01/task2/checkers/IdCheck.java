package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;

/**
 * Created by paul on 16.09.16.
 */
public class IdCheck implements Check {
    @Override
    public boolean check(Product p) throws ValidationException {
        if (p.getClientId() != null && p.getClientId() != "" && p.getClientId().equals(p.getClientId().toUpperCase())) {
            return true;
        } else {
            throw new ValidationException("Проверка Id не удалась: " + p.getClientId());
        }
    }
}
