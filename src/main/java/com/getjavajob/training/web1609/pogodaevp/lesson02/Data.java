package com.getjavajob.training.web1609.pogodaevp.lesson02;

import java.io.Serializable;
import java.util.List;

/**
 * Created by paul on 19.09.16.
 */
public class Data implements Serializable {
    private Organization organization;
    private OrgType orgType;
    private Human seo;
    private double capital;
    private String parentOrg;
    private List<Shareholder> list;

    public Organization getOrganization() {
        return organization;
    }

    public OrgType getOrgType() {
        return orgType;
    }

    public Human getSeo() {
        return seo;
    }

    public Double getCapital() {
        return capital;
    }

    public String getParentOrg() {
        return parentOrg;
    }

    public List<Shareholder> getList() {
        return list;
    }

    public Data() {
    }

    public Data(Organization organization) {
        this.organization = organization;
    }

    public void setList(List<Shareholder> list) {
        this.list = list;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public void setParentOrg(String parentOrg) {
        this.parentOrg = parentOrg;
    }

    public void setSeo(Human seo) {
        this.seo = seo;
    }

    public void setOrgType(OrgType orgType) {
        this.orgType = orgType;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "Data{" +
                "organization=" + organization +
                ", orgType=" + orgType +
                ", seo=" + seo +
                ", capital=" + capital +
                ", parentOrg='" + parentOrg + '\'' +
                ", list=" + list +
                '}';
    }
}
