package com.getjavajob.training.web1609.pogodaevp.lesson02;

import java.io.Serializable;

/**
 * Created by paul on 19.09.16.
 */
public enum OrgType implements Serializable {
    OAO("oao"),
    OOO("ooo"),
    ZAO("zao"),
    IP("ip");

    private String type;

    OrgType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static OrgType findByName(String string) {
        for (OrgType p : values()) {
            if (p.getType().equals(string)) {
                return p;
            }
        }
        return null;
    }
}
