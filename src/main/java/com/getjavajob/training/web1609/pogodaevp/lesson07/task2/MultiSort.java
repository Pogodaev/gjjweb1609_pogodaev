package com.getjavajob.training.web1609.pogodaevp.lesson07.task2;

/**
 * Created by paul on 10.10.16.
 */
public class MultiSort extends BasicSort {

    public <T extends Comparable<T>> void quicksort(final T[] a, final int left, final int right, final int tdepth) {
        if (right <= left) {
            return;
        }
        final int i = partition(a, left, right);

        if (tdepth < 4 && (i - left) > 1000) {
            final Thread t = new Thread() {
                public void run() {
                    quicksort(a, left, i - 1, tdepth + 1);
                }
            };
            t.start();
            quicksort(a, i + 1, right, tdepth + 1);

            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            quicksort(a, left, i - 1, tdepth);
            quicksort(a, i + 1, right, tdepth);
        }
    }
}
