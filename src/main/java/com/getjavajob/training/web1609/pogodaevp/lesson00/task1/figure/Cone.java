package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.figure;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis.CircleBasis;

/**
 * Created by paul on 13.09.16.
 */
public class Cone extends ConeLike implements Figure {

    public Cone(CircleBasis circleBasis, double height) {
        super(circleBasis, height);
    }

    public double getHeight() {
        return super.getHeight();
    }


    public void setHeight(int height) {
        super.setHeight(height);
    }

    public double getRadius() {
        return super.getBasis().returnSide();
    }

    @Override
    public double countSurfaceArea() {
        double r = getRadius();
        double h = getHeight();
        double secPart = Math.PI * r * Math.sqrt(r * r + h * h);
        return super.getBasis().countBasisArea() + secPart;
    }

}
