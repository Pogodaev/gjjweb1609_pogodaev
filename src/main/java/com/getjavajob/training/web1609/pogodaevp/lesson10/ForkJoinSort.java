package com.getjavajob.training.web1609.pogodaevp.lesson10;

import java.util.concurrent.RecursiveAction;


/**
 * Created by paul on 22.10.16.
 */
public class ForkJoinSort<T extends Comparable<T>> extends RecursiveAction {
    private T[] data;
    private int left;
    private int right;
    private int threshold = 10_000;

    public ForkJoinSort(T[] data) {
        this.data = data;
        this.left = 0;
        this.right = data.length - 1;
    }

    public ForkJoinSort(T[] data, int left, int right) {
        this.data = data;
        this.left = left;
        this.right = right;
    }

    @Override
    protected void compute() {
        SingleQuickSort<T> quickSort = new SingleQuickSort<>();
        if (right - left < threshold) {
            quickSort.sort(data, left, right);
        } else {
            int pivot = quickSort.partition(data, left, right);
            invokeAll(new ForkJoinSort(data, left, pivot - 1), new ForkJoinSort(data, pivot + 1, right));
        }
    }
}
