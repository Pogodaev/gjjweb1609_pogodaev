package com.getjavajob.training.web1609.pogodaevp.lesson15.task2;

import java.util.Random;

/**
 * Created by paul on 05.01.17.
 */
public class Philosoph implements Runnable {
    private Fork fork1;
    private Fork fork2;
    private Random random = new Random();

    public Philosoph(Fork fork1, Fork fork2) {
        this.fork1 = fork1;
        this.fork2 = fork2;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) { //thinkig
                Thread.sleep(random.nextInt(10));
                fork1.get();
                fork2.get();
                Thread.sleep(random.nextInt(10));//eating
                fork1.put();
                fork2.put();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
