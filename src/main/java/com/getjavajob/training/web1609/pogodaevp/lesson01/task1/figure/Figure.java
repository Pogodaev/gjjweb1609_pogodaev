package com.getjavajob.training.web1609.pogodaevp.lesson01.task1.figure;

/**
 * Created by paul on 13.09.16.
 */
public interface Figure {
    double countSurfaceArea();

    double countVolume();
}
