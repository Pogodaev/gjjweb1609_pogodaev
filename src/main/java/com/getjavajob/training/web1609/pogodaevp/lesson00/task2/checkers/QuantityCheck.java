package com.getjavajob.training.web1609.pogodaevp.lesson00.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task2.Product;

/**
 * Created by paul on 16.09.16.
 */
public class QuantityCheck implements Check {
    @Override
    public boolean check(Product p) {
        if (p.getNeedQuantity() > 0 && p.getNeedQuantity() < p.getStockQuantity()) {
            return true;
        }
        return false;
    }
}
