package com.getjavajob.training.web1609.pogodaevp.lesson03.task2;

import com.getjavajob.training.web1609.pogodaevp.lesson03.task2.annotations.XMLAlias;

/**
 * Created by paul on 09.10.16.
 */
@XMLAlias("Chelovek")
public class Human {

    private String family;

    public Human() {
    }

    public Human(String family) {
        this.family = family;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return "Human{" +
                "family='" + family + '\'' +
                '}';
    }
}
