package com.getjavajob.training.web1609.pogodaevp.lesson02;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.Serializable;

/**
 * Created by paul on 19.09.16.
 */
@XStreamAlias("shareholder")
public class Shareholder extends Human implements Serializable {
    private double percent;
    private Human human;

    public Shareholder() {
    }

    public void setHuman(Human human) {
        this.human = human;
    }

    public Shareholder(double percent, Human human) {
        this.percent = percent;
        this.human = human;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public Shareholder(String name, String surname, String lastname, double percent) {
        super(name, surname, lastname);
        this.percent = percent;
    }

    public Human getHuman() {
        return human;
    }

    @Override
    public String toString() {
        return "Shareholder{" +
                "percent=" + percent +
                ", human=" + human +
                '}';
    }
}
