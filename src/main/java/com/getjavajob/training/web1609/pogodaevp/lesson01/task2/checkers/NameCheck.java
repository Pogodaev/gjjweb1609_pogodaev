package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;

/**
 * Created by paul on 15.09.16.
 */
public class NameCheck implements Check {
    @Override
    public boolean check(Product product) throws ValidationException {
        if (product.getpName() != null && !product.getpName().equals("")) {
            return true;
        } else {
            throw new ValidationException("Название продукта не прошло проверку: " + product.getpName());
        }
    }
}
