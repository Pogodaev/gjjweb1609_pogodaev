package com.getjavajob.training.web1609.pogodaevp.lesson00.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task2.Product;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by paul on 16.09.16.
 */
public class EmailCheck implements Check {
    public static final Pattern VALID_EMAIL =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Override
    public boolean check(Product product) {
        Matcher matcher = VALID_EMAIL.matcher(product.getEmail());
        return matcher.find();
    }
}
