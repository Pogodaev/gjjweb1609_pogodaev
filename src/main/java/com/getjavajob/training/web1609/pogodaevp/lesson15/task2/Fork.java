package com.getjavajob.training.web1609.pogodaevp.lesson15.task2;

/**
 * Created by paul on 05.01.17.
 */
public class Fork {
    private static int I = 0;
    private int id = ++I;
    private boolean busy;

    public Fork() {
        busy = false;
    }

    public synchronized void get()  {
        while (busy) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName() + " get fork " + id);
        busy = true;
    }

    public synchronized void put() {
        busy = false;
        System.out.println(Thread.currentThread().getName() + " put fork " + id);
        notifyAll();
    }
}
