package com.getjavajob.training.web1609.pogodaevp.lesson03.task2;

import com.getjavajob.training.web1609.pogodaevp.lesson03.task2.annotations.XMLAlias;
import com.getjavajob.training.web1609.pogodaevp.lesson03.task2.annotations.XMLAttribute;
import com.getjavajob.training.web1609.pogodaevp.lesson03.task2.annotations.XMLTransient;

import javax.xml.bind.JAXBException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * Created by paul on 07.10.16.
 */
public class MyXstream {
    public static void main(String[] args) throws JAXBException, NoSuchFieldException, IllegalAccessException, IOException, NoSuchMethodException {

        //      Sample sample = new Sample("Hello", 12, new Human("OLeg"));
        //      System.out.println(toXML(sample));
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        fromXML(classLoader.getResource("dx.xml").openStream());
    }

    public static String toXML(Object o) throws JAXBException, NoSuchFieldException, IllegalAccessException {
        String tag = "";
        String ctag = "";
        String result = "";
        String willbe = "";
        String className = o.getClass().getName();
        Class clazz = o.getClass();
        Annotation[] anns = clazz.getAnnotations();

        for (Field field : clazz.getDeclaredFields()) {                   //идём по полям
            if (field.getAnnotation(XMLTransient.class) == null) {        //Сохраняем ли поле
                if (field.getAnnotation(XMLAlias.class) != null) {        //как его назовём
                    willbe = field.getAnnotation(XMLAlias.class).value(); //по Алиас
                } else {
                    willbe = field.getName();                             //field name
                }

                if (field.getAnnotation(XMLAttribute.class) != null) {    //если аттрибут
                    String att = willbe + "=\"" + field.getAnnotation(XMLAttribute.class).value() + "\"";
                    tag = addBraces(willbe + " " + att, true);
                } else {                                                  //если тэг
                    tag = addBraces(willbe, true);
                }
                ctag = addBraces(willbe, false);
                result += tag + "\n" + getField(o, field) + ctag + "\n";
            }
        }

        //последняя и первая скобка
        if (anns.length != 0) {
            String res = ((XMLAlias) anns[0]).value();
            result = addBraces(res, true) + "\n" + result;
            result += addBraces(res, false);
        } else {
            result = addBraces(className, true) + "\n" + result;
            result += addBraces(className, false);
        }

        return result;
    }

    public static Object fromXML(InputStream is) throws IOException, NoSuchMethodException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuffer sb = new StringBuffer();
        String line;
        String className;
        Class clazz = null;
        Object obj = null;
        ArrayList<String> list = new ArrayList<>();
        ArrayList<Field> fields = new ArrayList<>();


        while (reader.ready()) {
            line = reader.readLine();
            if (!line.contains("?xml")) {
                sb.append(line + "  ");
                System.out.println(line);
            }
        }

        String[] arr = sb.toString().split("[\\s]{2,}");
        for (String str : arr) {
            list.add(str);
        }

        className = replace(arr[0]);

        if (className.equals(replace(arr[arr.length - 1]))) {
            try {
                clazz = Class.forName(MyXstream.class.getPackage().getName() + "." + className);
                Constructor ctor = clazz.getConstructor();
                obj = ctor.newInstance();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            for (Annotation an : clazz.getDeclaredAnnotations()) {
                if (an instanceof XMLAlias) {
                    System.out.println(((XMLAlias) an).value());
                }
            }

            for (Field f : clazz.getDeclaredFields()) {
                fields.add(f);
            }


            for (Field f : fields) {
                Annotation[] ans = f.getDeclaredAnnotations();
                String alias = "";
                String attr = "";
                boolean trans = false;

                for (Annotation anot : ans) {
                    Class c = anot.annotationType();

                    if (c == XMLTransient.class) {
                        trans = true;
                    }
                    if (c == XMLAlias.class) {
                        alias = ((XMLAlias) anot).value();
                    }
                    if (c == XMLAttribute.class) {
                        attr = ((XMLAttribute) anot).value();
                    }
                }

                String match = "";
                if (!alias.equals("")) {
                    match = alias; //by Annotation
                } else {
                    match = f.getName(); //by name of the field
                }


                for (String l : list) {
                    if (l.contains(match) && !trans) {
                        int in = list.indexOf(l);
                        try {
                            f.setAccessible(true);
                            String s = list.get(in + 1);

                            if (f.getType().getSimpleName().equals("int")) {
                                f.set(obj, Integer.valueOf(s));
                                break;
                            } else if (f.getType().getSimpleName().equals("String")) {
                                f.set(obj, s);
                                break;
                            } else {
                                Object inner = getAnotherObject(f.getType().getCanonicalName(), list);
                                f.set(obj, inner);
                                break;
                            }
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return obj;
    }

    public static Object getAnotherObject(String path, ArrayList<String> list) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object obj = new Object();
        Class myClass = Class.forName(path);
        Field[] fields = myClass.getDeclaredFields();
        Constructor ctor = myClass.getConstructor();
        obj = ctor.newInstance();

        for (Field f : fields) {
            for (String k : list) {
                if (k.contains(f.getName())) {
                    int i = list.indexOf(k);
                    f.setAccessible(true);
                    f.set(obj, list.get(i + 1));
                    break;
                }
            }
        }
        return obj;
    }

    public static String replace(String str) {
        return str.replace("</", "").replace(">", "").replace("<", "").replace(" ", "");
    }


    public static String addBraces(String string, boolean b) {
        if (b) {
            return "<" + string + ">";
        } else {
            return "</" + string + ">";
        }
    }

    public static String getField(Object o, Field f) {
        String res = "";
        String type = f.getType().getSimpleName();

        if (type.equals("String")) {
            f.setAccessible(true);
            try {
                res = (String) f.get(o);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (type.equals("int")) {
            f.setAccessible(true);
            try {
                res = String.valueOf(f.getInt(o));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            try {
                f.setAccessible(true);
                res = toXML(f.get(o));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }
        return "    " + res + "\n";
    }
}
