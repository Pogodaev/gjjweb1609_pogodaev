package com.getjavajob.training.web1609.pogodaevp.lesson03.task1.readers;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

/**
 * Created by paul on 22.10.16.
 */
public interface XMLParser {
    void parse(String file) throws ParserConfigurationException, SAXException, IOException, XMLStreamException;
}
