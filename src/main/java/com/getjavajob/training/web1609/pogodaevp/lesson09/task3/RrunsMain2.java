package com.getjavajob.training.web1609.pogodaevp.lesson09.task3;

import java.util.Random;
import java.util.concurrent.Exchanger;

/**
 * Created by paul on 23.10.16.
 */
public class RrunsMain2 {
    private static final Exchanger<Integer> EXCHANGERODD = new Exchanger<>();
    private static final Exchanger<Integer> EXCHANGEREVEN = new Exchanger<>();
    private volatile static Integer number;

    public static void main(String[] args) {
        Generator gen = new Generator();
        gen.start();
        new Consumer(EXCHANGERODD).start();
        new Consumer(EXCHANGEREVEN).start();
    }

    private static class Generator extends Thread {
        Random r = new Random();

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    number = r.nextInt();
                    System.out.println("Генерирую число... " + number);
                    if (number % 2 == 1) {
                        EXCHANGEREVEN.exchange(number);
                    } else {
                        EXCHANGERODD.exchange(number);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static class Consumer extends Thread {
        private Exchanger<Integer> exchanger;

        public Consumer(Exchanger<Integer> exchanger) {
            this.exchanger = exchanger;
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    sleep(300);
                    System.out.println(Thread.currentThread().getName() + " потребил " + exchanger.exchange(0));
                    sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
