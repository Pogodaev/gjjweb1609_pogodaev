package com.getjavajob.training.web1609.pogodaevp.lesson02.IOFilesClass;

import com.getjavajob.training.web1609.pogodaevp.lesson02.Datas;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 * Created by paul on 27.09.16.
 */
public class LoadStart {
    private Datas datas;

    public LoadStart() {
    }

    public Datas getDatas() {
        return datas;
    }

    public void setDatas(Datas datas) {
        this.datas = datas;
    }

    public static Datas loadFromFile(String str) {
        Datas data = null;
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream("oOrganizations.dat")))) {
            data = (Datas) ois.readObject();
        } catch (Exception e) {
            System.out.println("Fail");
        }
        return data;
    }
}
