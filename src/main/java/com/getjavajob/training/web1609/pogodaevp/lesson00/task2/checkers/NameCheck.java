package com.getjavajob.training.web1609.pogodaevp.lesson00.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task2.Product;

/**
 * Created by paul on 15.09.16.
 */
public class NameCheck implements Check {
    @Override
    public boolean check(Product product) {
        if (product.getpName() != null && !product.getpName().equals("")) {
            return true;
        }
        return false;
    }
}
