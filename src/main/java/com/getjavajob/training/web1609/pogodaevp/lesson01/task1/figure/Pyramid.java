package com.getjavajob.training.web1609.pogodaevp.lesson01.task1.figure;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task1.basis.PyramidBasis;

/**
 * Created by paul on 13.09.16.
 */
public class Pyramid implements Figure, Cloneable {
    private int corners;
    private double height;
    private double border;
    public PyramidBasis pyramidBasis;

    @Override
    protected Pyramid clone() {
        try {
            Pyramid copy = (Pyramid) super.clone();
            copy.pyramidBasis = copy.pyramidBasis.clone();
            return copy;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return "Pyramid{" +
                "corners=" + corners +
                ", height=" + height +
                ", border=" + border +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pyramid pyramid = (Pyramid) o;

        if (getCorners() != pyramid.getCorners()) return false;
        if (Double.compare(pyramid.getHeight(), getHeight()) != 0) return false;
        return Double.compare(pyramid.getBorder(), getBorder()) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getCorners();
        temp = Double.doubleToLongBits(getHeight());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getBorder());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public Pyramid(PyramidBasis pyramidBasis, double height) {
        this.pyramidBasis = pyramidBasis;
        this.height = height;
        this.border = pyramidBasis.getBorder();
        this.corners = pyramidBasis.getCorners();
    }

    public int getCorners() {
        return corners;
    }

    public void setCorners(int corners) {
        this.corners = corners;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getBorder() {
        return border;
    }

    public void setBorder(double border) {
        this.border = border;
    }

    public Pyramid(int corners, double height, double border) {
        this.corners = corners;
        this.height = height;
        this.border = border;
    }

    @Override
    public double countSurfaceArea() {
        double h = getHeight();
        double r = (2 * pyramidBasis.countBasisArea()) / (getCorners() * getBorder());
        double apofema = Math.sqrt(h * h + r * r);
        return pyramidBasis.countBasisArea() + 0.5 * getBorder() * getCorners() * apofema;
    }

    @Override
    public double countVolume() {
        return (1.0 / 3.0) * pyramidBasis.countBasisArea() * getHeight();
    }
}
