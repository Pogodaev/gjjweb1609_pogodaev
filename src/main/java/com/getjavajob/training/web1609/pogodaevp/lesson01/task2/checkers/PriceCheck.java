package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;

/**
 * Created by paul on 16.09.16.
 */
public class PriceCheck implements Check {
    @Override
    public boolean check(Product p) throws ValidationException {
        if (p.getPrice() > 0) {
            return true;
        } else {
            throw new ValidationException("Проверка цены не пройдена: " + p.getPrice());
        }
    }
}
