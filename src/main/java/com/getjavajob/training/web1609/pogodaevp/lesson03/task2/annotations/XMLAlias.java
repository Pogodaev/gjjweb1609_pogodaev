package com.getjavajob.training.web1609.pogodaevp.lesson03.task2.annotations;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by paul on 07.10.16.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface XMLAlias {
    String value();
}
