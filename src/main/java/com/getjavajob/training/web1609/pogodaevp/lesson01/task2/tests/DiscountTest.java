package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.tests;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Validator;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.Check;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.DiscountCheck;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by paul on 12.10.16.
 */
public class DiscountTest {
    private Product product;
    private Validator validator;

    @Before
    public void init() {
        System.out.println("Before");
        ArrayList<Check> list = new ArrayList<>();
        list.add(new DiscountCheck());
        this.product = new Product();
        this.validator = new Validator(list);
    }

    @Test
    public void testDiscount() throws ValidationException {
        new DiscountCheck().check(this.product);
    }

    @Test(expected = ValidationException.class)
    public void testDiscountFail() throws ValidationException {
        this.product.setPrice(5);
        this.product.setDiscount(3);
        this.validator.validate(this.product);
    }
}
