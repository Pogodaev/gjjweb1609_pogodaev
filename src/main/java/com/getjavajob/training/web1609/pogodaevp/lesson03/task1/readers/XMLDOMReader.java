package com.getjavajob.training.web1609.pogodaevp.lesson03.task1.readers;

import com.getjavajob.training.web1609.pogodaevp.lesson02.Human;
import com.getjavajob.training.web1609.pogodaevp.lesson02.OrgType;
import com.getjavajob.training.web1609.pogodaevp.lesson02.Organization;
import com.getjavajob.training.web1609.pogodaevp.lesson02.Shareholder;
import com.getjavajob.training.web1609.pogodaevp.lesson03.task1.Data;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by paul on 02.10.16.
 */
public class XMLDOMReader implements XMLParser {

    @Override
    public void parse(String file) throws ParserConfigurationException, SAXException, IOException, XMLStreamException {
        ArrayList<Shareholder> shareholders = new ArrayList<>();
        Data data = new Data();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(ClassLoader.getSystemResourceAsStream(file));

        Element element = document.getDocumentElement();
        NodeList nodes = element.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node instanceof Element && node.getNodeName().equals("data")) {

                NodeList childNodes = node.getChildNodes();
                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node cNode = childNodes.item(j);

                    if (cNode instanceof Element) {
                        String content = cNode.getTextContent().trim().replaceFirst("^([\\W]+)<", "<");
                        switch (cNode.getNodeName()) {
                            case "organization":
                                data.setOrganization(new Organization(content));
                                break;
                            case "orgType":
                                data.setOrgType(OrgType.findByName(content.toLowerCase()));
                                break;
                            case "seo":
                                Human h = new Human();
                                NodeList ns = cNode.getChildNodes();
                                ArrayList<String> ar = new ArrayList<>();
                                for (int k = 1; k < ns.getLength(); k += 2) {
                                    ar.add(ns.item(k).getTextContent());
                                }
                                h.setName(ar.get(0));
                                h.setSurname(ar.get(1));
                                h.setLastname(ar.get(2));
                                data.setSeo(h);
                                break;
                            case "capital":
                                data.setCapital(Double.parseDouble(content));
                                break;
                            case "parent":
                                data.setParentOrg(content);
                                break;
                            case "shareholder":
                                Shareholder shareholder = new Shareholder();
                                NodeList nodeList1 = cNode.getChildNodes();
                                for (int k = 1; k < nodeList1.getLength(); k += 2) {
                                    if (nodeList1.item(k).getNodeName().trim().equals("percent")) {
                                        shareholder.setPercent(Double.parseDouble(nodeList1.item(k).getTextContent()));
                                    }
                                    if (nodeList1.item(k).getNodeName().trim().equals("name")) {
                                        shareholder.setHuman(new Human(nodeList1.item(k).getTextContent(), "", ""));
                                    }
                                }
                                shareholders.add(shareholder);
                                break;
                        }
                    }
                    data.setList(shareholders);
                }
                System.out.println(data);
            }
        }
    }
}
