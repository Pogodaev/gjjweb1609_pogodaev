package com.getjavajob.training.web1609.pogodaevp.lesson00.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task2.Product;

/**
 * Created by paul on 16.09.16.
 */
public class DiscountCheck implements Check {
    private final static double DISCOUNT = 0.2;

    @Override
    public boolean check(Product p) {
        if (p.getDiscount() > 0 && p.getDiscount() <= (p.getPrice() * p.getNeedQuantity()) * DISCOUNT) {
            return true;
        }
        return false;
    }
}
