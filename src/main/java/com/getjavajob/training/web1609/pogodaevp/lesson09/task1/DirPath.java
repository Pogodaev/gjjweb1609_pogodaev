package com.getjavajob.training.web1609.pogodaevp.lesson09.task1;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by paul on 22.10.16.
 */
public class DirPath {
    /**
     * Enter
     * /home/paul
     * Extension
     * txt
     * rw- 6616b    guvcview_image-3.jpg           Thu Apr 07 20:53:59 MSK 2016
     * rwx  d     IdeaProjects           Tue Sep 27 00:08:46 MSK 2016
     * rwx  d     Изображения           Sat Oct 22 08:51:45 MSK 2016
     * rw- 11550b    .ICEauthority           Sat Oct 22 05:51:59 MSK 2016
     * rw- 496128b    ExamSolution.doc           Mon Sep 05 19:00:22 MSK 2016
     * rwx  d     .cache           Wed Aug 31 22:02:17 MSK 2016
     * ---  d     .dbus           Thu Mar 17 02:04:25 MSK 2016
     */
    public static void main(String[] args) throws IOException {
        String path = "";
        String ext = "";
        String result = "";
        ArrayList<File> files = new ArrayList<>();
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter");
        path = bf.readLine();

        System.out.println("Extension ");
        ext = bf.readLine();
        bf.close();

        File dir = new File(path);
        if (!dir.exists()) {
            throw new IllegalArgumentException();
        }
        if (dir.exists() && dir.isDirectory()) {
            String[] filesInDir = dir.list();
            for (String f : filesInDir) {
                files.add(new File(path + "/" + f));
            }
        }

        if (ext.equals("")) {
            for (File f : files) {
                if (f.isDirectory()) {
                    printInside(f, ext);
                    result += " d     " + f.getName();
                } else {
                    result += f.getName();
                }
                if (f.isFile()) {
                    result = f.length() + "b    " + result;  //
                }
                result = rwx(f) + result;
                result += "           " + new Date(f.lastModified());

                System.out.println(result);
                result = "";
            }
        } else {
            for (File s : finder(dir, ext)) {
                System.out.println(s.getName());
            }
        }
    }

    private static void printInside(File f, String ext) {
        for (File file : finder(f, ext)) {
            System.out.println(file.getName());
        }
    }

    public static File[] finder(File d, final String ex) {
        return d.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.endsWith("." + ex);
            }
        });
    }

    public static String rwx(File f) {
        String str = "";
        if (f.canExecute()) {
            str = "x " + str;
        } else {
            str = "- " + str;
        }
        if (f.canWrite()) {
            str = "w" + str;
        } else {
            str = "-" + str;
        }
        if (f.canRead()) {
            str = "r" + str;
        } else {
            str = "-" + str;
        }
        return str;
    }
}
