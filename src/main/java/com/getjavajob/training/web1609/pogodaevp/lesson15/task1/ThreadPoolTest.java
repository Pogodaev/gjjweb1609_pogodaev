package com.getjavajob.training.web1609.pogodaevp.lesson15.task1;

import com.getjavajob.training.web1609.pogodaevp.lesson15.task2.Fork;
import com.getjavajob.training.web1609.pogodaevp.lesson15.task2.Philosoph;


/**
 * Created by paul on 30.01.17.
 */
public class ThreadPoolTest {

    public void testPoolStart() {
        ThreadPool pool = new ThreadPool(4, 1);
        Fork f1 = new Fork();
        Fork f2 = new Fork();
        Fork f3 = new Fork();
        Fork f4 = new Fork();
        Fork f5 = new Fork();

        try {
            pool.execute(new Philosoph(f5, f1));
            pool.execute(new Philosoph(f1, f2));
            pool.execute(new Philosoph(f3, f2));
            pool.execute(new Philosoph(f3, f4));
            pool.execute(new Philosoph(f5, f4));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
