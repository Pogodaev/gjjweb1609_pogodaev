package com.getjavajob.training.web1609.pogodaevp.lesson08;

/**
 * Created by paul on 18.10.16.
 */
public class OneThreadMultiple extends ParentMatrix {
    public static int[][] multiplicar(int[][] A, int[][] B) throws InterruptedException {
        int aRows = A.length;
        int aColumns = A[0].length;
        int bRows = B.length;
        int bColumns = B[0].length;

        if (aColumns != bRows) {
            throw new IllegalArgumentException();
        }

        int[][] C = new int[aRows][bColumns];

        for (int i = 0; i < aRows; i++) {
            for (int j = 0; j < bColumns; j++) {
                mult(i, j, A, B, C);
            }
        }
        return C;
    }
}
