package com.getjavajob.training.web1609.pogodaevp.lesson01.task1.figure;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task1.basis.CylinderBasis;

/**
 * Created by paul on 13.09.16.
 */
public class Cylinder implements Figure, Cloneable {
    private double radius;
    private double height;
    public CylinderBasis cylinderBasis;

    @Override
    protected Cylinder clone() {
        try {
            Cylinder copy = (Cylinder) super.clone();
            copy.cylinderBasis = copy.cylinderBasis.clone();
            return copy;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return "Cylinder{" +
                "radius=" + radius +
                ", height=" + height +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cylinder cylinder = (Cylinder) o;

        if (Double.compare(cylinder.getRadius(), getRadius()) != 0) return false;
        return Double.compare(cylinder.getHeight(), getHeight()) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getRadius());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getHeight());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public Cylinder(CylinderBasis cylinderBasis) {
        this.cylinderBasis = cylinderBasis;
        this.radius = cylinderBasis.getRadius();
        this.height = cylinderBasis.getHeight();
    }

    public Cylinder(double height, double radius) {
        this.height = height;
        this.radius = radius;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double countSurfaceArea() {
        double secPart = 2 * Math.PI * getHeight() * getRadius();
        return 2 * cylinderBasis.countBasisArea() + secPart;
    }

    @Override
    public double countVolume() {
        return cylinderBasis.countBasisArea() * getHeight();
    }
}
