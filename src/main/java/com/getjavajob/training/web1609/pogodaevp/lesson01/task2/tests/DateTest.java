package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.tests;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Validator;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.Check;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.DateCheck;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by paul on 12.10.16.
 */
public class DateTest {
    private Product product;
    private Validator validator;

    @Before
    public void init() throws ParseException {
        System.out.println("Before");
        ArrayList<Check> list = new ArrayList<>();
        list.add(new DateCheck());
        this.product = new Product();
        this.validator = new Validator(list);
    }

    @Test(expected = NullPointerException.class)
    public void dateTest() throws ValidationException {
        new DateCheck().check(this.product);
    }

    @Test(expected = ValidationException.class)
    public void dateTestPass() throws ParseException, ValidationException {
        SimpleDateFormat fmt = new SimpleDateFormat("dd.MM.yyyy");
        Date date = fmt.parse("17.09.2016");
        this.product.setDate(date);
        this.validator.validate(this.product);
    }
}
