package com.getjavajob.training.web1609.pogodaevp.lesson01.task1.figure;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task1.basis.ConeBasis;

/**
 * Created by paul on 13.09.16.
 */
public class Cone implements Figure, Cloneable {
    private double height;
    private double radius;
    public ConeBasis coneBasis;

    @Override
    protected Cone clone() {
        try {
            Cone copy = (Cone) super.clone();
            copy.coneBasis = copy.coneBasis.clone();
            return copy;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cone cone = (Cone) o;

        if (Double.compare(cone.getHeight(), getHeight()) != 0) return false;
        return Double.compare(cone.getRadius(), getRadius()) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getHeight());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getRadius());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public Cone(ConeBasis coneBasis) {
        this.height = coneBasis.getHeight();
        this.radius = coneBasis.getRadius();
        this.coneBasis = coneBasis;
    }

    @Override
    public String toString() {
        return "Cone{" +
                "radius=" + radius +
                ", height=" + height +
                '}';
    }


    public Cone(double height, double radius) {
        this.height = height;
        this.radius = radius;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public double countSurfaceArea() {
        double r = getRadius();
        double h = getHeight();
        double secPart = Math.PI * r * Math.sqrt(r * r + h * h);
        return coneBasis.countBasisArea() + secPart;
    }

    @Override
    public double countVolume() {
        return (1.0 / 3.0) * getHeight() * coneBasis.countBasisArea();
    }
}
