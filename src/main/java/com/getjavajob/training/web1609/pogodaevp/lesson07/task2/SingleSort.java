package com.getjavajob.training.web1609.pogodaevp.lesson07.task2;

/**
 * Created by paul on 10.10.16.
 */
public class SingleSort extends BasicSort {
    private Thing[] things;
    private int n;

    public void sort(Thing[] values) {
        if (values == null || values.length == 0) {
            return;
        }
        this.things = values;
        n = values.length;
        quicksort(0, n - 1);
    }

    private void quicksort(int low, int high) {
        if (high <= low) {
            return;
        }

        int k = partition(things, low, high);

        if (low < k) {
            quicksort(low, k - 1);
        }
        if (k < high) {
            quicksort(k + 1, high);
        }
    }
}
