package com.getjavajob.training.web1609.pogodaevp.lesson15.task1;

import java.util.LinkedList;
import java.util.List;

public class BlockingQueue {
    private List queue = new LinkedList();
    private int limit = 10;

    public BlockingQueue() {
    }

    public BlockingQueue(int limit) {
        this.limit = limit;
    }
    //добавляем таск в очередь
    public synchronized void enqueue(Object item) throws InterruptedException {
        while (queue.size() == limit) { // если очередь под завязку-ждем
            wait();
        }

        if (queue.size() == 0) {
            notifyAll();
        }
        queue.add(item);
    }

    //освобождаем элемент из очереди
    public synchronized Object dequeue() throws InterruptedException {
        while (queue.size() == 0) {
            wait();
        }

        if (queue.size() == limit) {
            notifyAll();
        }
        return queue.remove(0);
    }
}
