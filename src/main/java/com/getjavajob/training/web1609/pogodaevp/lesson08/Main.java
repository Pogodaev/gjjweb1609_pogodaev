package com.getjavajob.training.web1609.pogodaevp.lesson08;

import java.io.IOException;


/**
 * Created by paul on 22.10.16.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        MatrixThreadMultiple m = new MatrixThreadMultiple();
        int[][] a = m.readFromFile("matrix1.txt");
        int[][] b = m.readFromFile("matrix2.txt");
        int[][] c = m.multiple(a, b);
        m.printMatrix(c);
    }
}
