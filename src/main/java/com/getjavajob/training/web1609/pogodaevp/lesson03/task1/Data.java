package com.getjavajob.training.web1609.pogodaevp.lesson03.task1;

import com.getjavajob.training.web1609.pogodaevp.lesson02.Human;
import com.getjavajob.training.web1609.pogodaevp.lesson02.OrgType;
import com.getjavajob.training.web1609.pogodaevp.lesson02.Organization;
import com.getjavajob.training.web1609.pogodaevp.lesson02.Shareholder;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.io.Serializable;
import java.util.List;

/**
 * Created by paul on 19.09.16.
 */
@XStreamAlias("data")
public class Data implements Serializable {
    @XStreamAlias("organization")
    private Organization organization;
    @XStreamAlias("orgType")
    private OrgType orgType;
    @XStreamAlias("seo")
    private Human seo;
    @XStreamAlias("capital")
    private Double capital;
    @XStreamAlias("parent")
    private String parentOrg;
    @XStreamImplicit(itemFieldName = "shareholder")
    private List<Shareholder> list;


    public Organization getOrganization() {
        return organization;
    }

    public OrgType getOrgType() {
        return orgType;
    }

    public Human getSeo() {
        return seo;
    }

    public Double getCapital() {
        return capital;
    }

    public String getParentOrg() {
        return parentOrg;
    }

    public List<Shareholder> getList() {
        return list;
    }

    public Data() {
    }

    public Data(com.getjavajob.training.web1609.pogodaevp.lesson02.Organization organization) {
        this.organization = organization;
    }

    public void setList(List<Shareholder> list) {
        this.list = list;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public void setParentOrg(String parentOrg) {
        this.parentOrg = parentOrg;
    }

    public void setSeo(Human seo) {
        this.seo = seo;
    }

    public void setOrgType(OrgType orgType) {
        this.orgType = orgType;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "Data{" +
                "organization=" + organization +
                ", orgType=" + orgType +
                ", seo=" + seo +
                ", capital=" + capital +
                ", parentOrg='" + parentOrg + '\'' +
                ", list=" + list +
                '}';
    }
}
