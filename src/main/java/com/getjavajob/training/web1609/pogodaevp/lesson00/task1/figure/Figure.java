package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.figure;

/**
 * Created by paul on 13.09.16.
 */
public interface Figure {
    double countSurfaceArea();

    double countCrossSection(int h);

    double countVolume();
}
