package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.figure;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis.CircleBasis;

/**
 * Created by paul on 13.09.16.
 */
public class Cylinder extends StrightLike implements Figure {

    public Cylinder(CircleBasis cylinderBasis, double height) {
        super(cylinderBasis, height);
    }

    public double getHeight() {
        return super.getHeight();
    }

    public void setHeight(double height) {
        super.setHeight(height);
    }

    public double getRadius() {
        return super.getBasis().returnSide();
    }

    @Override
    public double countSurfaceArea() {
        double secPart = 2 * Math.PI * getHeight() * getRadius();
        return 2 * super.getBasis().countBasisArea() + secPart;
    }
}
