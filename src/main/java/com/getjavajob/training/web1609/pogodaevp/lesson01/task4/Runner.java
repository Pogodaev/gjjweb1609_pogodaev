package com.getjavajob.training.web1609.pogodaevp.lesson01.task4;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by paul on 19.09.16.
 */
public class Runner {
    public static void main(String[] args) throws ParseException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Product product = new Product();
        product.setpName("nom nom");
        product.setStockQuantity(10);
        product.setNeedQuantity(5);
        product.setPrice(120);
        product.setDiscount(2);
        product.setClientId("OPQWPPPPPPPPPP");
        product.setEmail("test@gmail.com");
        product.setPhone("+790000000");
        SimpleDateFormat fmt = new SimpleDateFormat("dd.MM.yyyy");
        Date date = fmt.parse("17.10.2016");
        product.setDate(date);

        Class<Product> clazz = Product.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(SimpleStringAnnotation.class)) {
                SimpleStringAnnotation simpleStringAnnotation = field.getAnnotation(SimpleStringAnnotation.class);
                System.out.println("Поле " + field.getName() + " " + simpleStringAnnotation.name());
            }
        }
    }
}
