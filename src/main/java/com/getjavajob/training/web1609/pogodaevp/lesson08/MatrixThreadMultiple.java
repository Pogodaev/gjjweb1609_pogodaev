package com.getjavajob.training.web1609.pogodaevp.lesson08;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul on 22.10.16.
 */
public class MatrixThreadMultiple extends ParentMatrix {
    public static int NUM_OF_THREADS;

    public static int[][] multiple(int[][] a, int[][] b) {
        int row, col;
        int[][] c = new int[a.length][b[0].length];
        NUM_OF_THREADS = a.length * b[0].length;
        Thread[] thrd = new Thread[NUM_OF_THREADS];
        int i = 0;
        List<String> list = new ArrayList<>();

        for (row = 0; row < a.length; row++) {
            for (col = 0; col < b[0].length; col++) {
                thrd[i++] = new Thread(new Runner(row, col, a, b, c));
                thrd[i - 1].start();
            }
        }

        for (Thread t : thrd) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return c;
    }
}
