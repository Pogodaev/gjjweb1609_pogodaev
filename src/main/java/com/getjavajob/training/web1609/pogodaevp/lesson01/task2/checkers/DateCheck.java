package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;

import java.util.Date;

/**
 * Created by paul on 16.09.16.
 */
public class DateCheck implements Check {
    @Override
    public boolean check(Product product) throws ValidationException {
        int days = (int) (product.getDate().getTime() / (1000 * 60 * 60 * 24));
        int nowDays = (int) (new Date().getTime() / (1000 * 60 * 60 * 24));

        if (days - nowDays >= 1) {
            return true;
        } else {
            throw new ValidationException("Плохая дата: " + product.getDate());
        }
    }
}
