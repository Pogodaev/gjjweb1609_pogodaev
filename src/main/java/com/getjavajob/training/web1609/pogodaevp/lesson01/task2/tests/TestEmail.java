package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.tests;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Validator;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.Check;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.EmailCheck;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by paul on 12.10.16.
 */
public class TestEmail {
    private Product product;
    private Validator validator;

    @Before
    public void init() {
        System.out.println("Before");
        ArrayList<Check> list = new ArrayList<>();
        list.add(new EmailCheck());
        this.product = new Product();
        this.validator = new Validator(list);
    }

    @Test
    public void testEmail() throws ValidationException {
        this.product.setEmail("test@gmail.com");
        assertEquals(true, new EmailCheck().check(this.product));
    }

    @Test(expected = ValidationException.class)
    public void testEmailFalse() throws ValidationException {
        this.product.setEmail("test@gmailcom");
        this.validator.validate(this.product);
    }
}
