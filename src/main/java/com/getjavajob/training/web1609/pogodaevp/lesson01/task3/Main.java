package com.getjavajob.training.web1609.pogodaevp.lesson01.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by paul on 18.09.16.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter country name");
        String searchString = bf.readLine();
        bf.close();

        Parts part = Parts.valueOf(searchString);
        if (part != null) {
            System.out.println("Area : " + part.getArea() + "\n"
                    + "Countries number : " + part.getCountriesNumber() + "\n"
                    + "Population : " + part.getPopulation());
        }
    }
}
