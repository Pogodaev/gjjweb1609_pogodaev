package com.getjavajob.training.web1609.pogodaevp.lesson01.task1.basis;

/**
 * Created by paul on 13.09.16.
 */
public class PyramidBasis extends AbstractBasis implements Cloneable {
    private int corners;
    private double height;
    private double border;

    @Override
    public PyramidBasis clone() {
        try {
            return (PyramidBasis) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public PyramidBasis(int corners, double height, double border) {
        this.corners = corners;
        this.height = height;
        this.border = border;
    }

    public int getCorners() {
        return corners;
    }

    public void setCorners(int corners) {
        this.corners = corners;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getBorder() {
        return border;
    }

    public void setBorder(double border) {
        this.border = border;
    }

    @Override
    public double countBasisArea() {
        return countCornerArea(getCorners(), getBorder());
    }

    @Override
    public double countCrossSection(double crossHeight) {
        double d = getHeight();
        return (countBasisArea() * crossHeight * crossHeight) / (d * d);
    }
}
