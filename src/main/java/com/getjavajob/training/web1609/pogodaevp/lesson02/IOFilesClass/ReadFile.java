package com.getjavajob.training.web1609.pogodaevp.lesson02.IOFilesClass;

import com.getjavajob.training.web1609.pogodaevp.lesson02.*;
import com.getjavajob.training.web1609.pogodaevp.lesson02.exceptions.CapitalException;
import com.getjavajob.training.web1609.pogodaevp.lesson02.exceptions.FileFormatException;
import com.getjavajob.training.web1609.pogodaevp.lesson02.exceptions.OrgTypeException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul on 23.09.16.
 */
public class ReadFile {
    public List<Data> readFromFile(String txt) throws IOException, FileFormatException, OrgTypeException, CapitalException {
        List<Data> list = new ArrayList<>();
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        int length = 0;

        System.out.println("Enter filename *.txt");
        String path = bf.readLine();
        bf.close();

        //Here we will read our file
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), Charset.forName("UTF-8")))) {
            String line = "";
            int i = 0;
            while (br.ready()) {
                line = br.readLine();
                list.add(stringToData(line));
            }

        } catch (IOException e) {
            System.out.println("Catch Error");
            e.printStackTrace();
        }

        return list;
    }

    public static Data stringToData(String line) throws OrgTypeException, CapitalException, FileFormatException {


        String[] strings = line.split(" ");
        if (strings.length < 7) {
            throw new FileFormatException();
        }


        //Data txt format:
        //MyOrganization ooo Ivanov Ivan Ivanich 123000 NONE Kumis 20 Lesha 30
        //org
        Data data = new Data();
        data.setOrganization(new Organization(strings[0]));
        //org type
        OrgType org = OrgType.findByName(strings[1]);
        if (org == null) {
            throw new OrgTypeException();
        } else {
            data.setOrgType(org);
        }
        Human seo = new Human(strings[2], strings[3], strings[4]);
        data.setSeo(seo);

        try {
            data.setCapital(Double.parseDouble(strings[5]));
        } catch (Exception e) {
            throw new CapitalException();
        }


        data.setParentOrg(strings[6]);

        ArrayList<Shareholder> shareholders = new ArrayList<>();

        for (int j = 7; j < strings.length; j++) {
            Double db = Double.parseDouble(strings[j + 1]);
            Shareholder shareholder = new Shareholder(db, new Human(strings[j], "", ""));
            shareholders.add(shareholder);
        }
        data.setList(shareholders);

        return data;
    }
}
