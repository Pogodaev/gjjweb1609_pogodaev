import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;

/**
 * Created by paul on 27.11.16.
 * Написать консольное приложения для подключения к MySQL и выполнения запросов.
 * То есть создать свой аналог MySQL Client.
 */
public class MySQL {
    public static void main(String[] args) throws ClassNotFoundException {
       askUser();
    }

    public static void askUser() throws ClassNotFoundException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String command = "";

        SqlClient sqlClient = new SqlClient();
        try {
            System.out.println("Enter your SQL script or exit for quit");
            command = br.readLine();
            while (!command.equals("exit")) {
                sqlClient.execute(command);
                System.out.println("Enter your SQL script or exit for quit");
                command = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                sqlClient.closeConnection();
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
