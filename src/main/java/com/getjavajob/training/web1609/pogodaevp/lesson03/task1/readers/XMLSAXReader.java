package com.getjavajob.training.web1609.pogodaevp.lesson03.task1.readers;

import com.getjavajob.training.web1609.pogodaevp.lesson03.task1.Data;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Created by paul on 04.10.16.
 */

public class XMLSAXReader implements XMLParser {
    static ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

    @Override
    public void parse(String file) throws ParserConfigurationException, SAXException, IOException {
        System.out.println("Hello, now will test SAX Parser");
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        SAXParser parser = parserFactory.newSAXParser();
        XMLSAX handler = new XMLSAX();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        parser.parse(classLoader.getResource(file).getFile(), handler);

        for (Data d : handler.getDatas()) {
            System.out.println(d);
        }
    }
}

