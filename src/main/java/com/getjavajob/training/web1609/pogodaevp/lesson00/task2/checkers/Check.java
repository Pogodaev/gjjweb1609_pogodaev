package com.getjavajob.training.web1609.pogodaevp.lesson00.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task2.Product;

/**
 * Created by paul on 15.09.16.
 */
public interface Check {
    boolean check(Product product);
}
