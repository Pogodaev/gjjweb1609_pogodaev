package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.tests;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Validator;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.Check;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.QuantityCheck;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by paul on 12.10.16.
 */
public class QuantityTest {
    private Product product;
    private Validator validator;

    @Before
    public void init() {
        System.out.println("Before");
        ArrayList<Check> list = new ArrayList<>();
        list.add(new QuantityCheck());
        this.product = new Product();
        this.validator = new Validator(list);
    }

    @Test
    public void testQuantity() throws ValidationException {
        this.product.setNeedQuantity(1);
        this.product.setStockQuantity(3);
        assertEquals(true, new QuantityCheck().check(this.product));
    }

    @Test(expected = ValidationException.class)
    public void testQuantityFail() throws ValidationException {
        this.product.setNeedQuantity(-1);
        this.validator.validate(this.product);
    }
}
