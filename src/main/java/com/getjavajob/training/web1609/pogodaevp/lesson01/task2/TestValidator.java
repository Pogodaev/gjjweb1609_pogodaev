package com.getjavajob.training.web1609.pogodaevp.lesson01.task2;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by paul on 13.09.16.
 */
public class TestValidator {
    public static void main(String[] args) throws ParseException {
        Product product = new Product();
        product.setpName("nom nom");
        product.setStockQuantity(10);
        product.setNeedQuantity(5);
        product.setPrice(120);
        product.setDiscount(2);
        product.setClientId("OPQW");
        product.setEmail("test@gmail.com");
        product.setPhone("+790000000");
        SimpleDateFormat fmt = new SimpleDateFormat("dd.MM.yyyy");
        Date date = fmt.parse("17.10.2016");
        product.setDate(date);
        ArrayList<Check> arrayList = new ArrayList<>();
        arrayList.add(new NameCheck());
        arrayList.add(new DateCheck());
        arrayList.add(new EmailCheck());
        arrayList.add(new IdCheck());
        arrayList.add(new PriceCheck());
        arrayList.add(new PhoneCheck());
        arrayList.add(new QuantityCheck());
        arrayList.add(new DiscountCheck());
        Validator val = new Validator(arrayList);
        val.validate(product);
    }
}
