package com.getjavajob.training.web1609.pogodaevp.lesson03.task1;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by paul on 07.10.16.
 */
@XStreamAlias("datas")
public class Datas {
    @XStreamImplicit(itemFieldName = "data")
    private List<Data> list;

    public Datas(List<Data> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Datas{" +
                "list=" + list +
                '}';
    }
}
