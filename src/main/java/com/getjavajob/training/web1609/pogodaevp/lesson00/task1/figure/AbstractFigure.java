package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.figure;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis.AbstractBasis;
import com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis.Basis;

/**
 * Created by paul on 18.10.16.
 */
public abstract class AbstractFigure implements Figure {
    private AbstractBasis basis;
    private double height;

    public AbstractFigure(AbstractBasis basis, double height) {
        this.basis = basis;
        this.height = height;
    }

    public Basis getBasis() {
        return basis;
    }

    public void setBasis(AbstractBasis basis) {
        this.basis = basis;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public abstract double countCrossSection(int h);
}
