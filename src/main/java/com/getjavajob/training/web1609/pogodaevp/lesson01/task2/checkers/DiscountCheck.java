package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;

/**
 * Created by paul on 16.09.16.
 */
public class DiscountCheck implements Check {
    @Override
    public boolean check(Product p) throws ValidationException {
        if (p.getDiscount() > 0 && p.getDiscount() <= (p.getPrice() * p.getNeedQuantity()) / 5) {
            return true;
        } else {
            throw new ValidationException("Скидка более 20% от стоимости");
        }
    }
}
