package com.getjavajob.training.web1609.pogodaevp.lesson07.task2;

/**
 * Created by paul on 10.10.16.
 */
public class Thing implements Comparable<Thing> {

    private String name;

    public Thing(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Thing thing) {
        return name.compareTo(thing.getName());
    }

    @Override
    public String toString() {
        return "Thing{" +
                "name='" + name + '\'' +
                '}';
    }
}
