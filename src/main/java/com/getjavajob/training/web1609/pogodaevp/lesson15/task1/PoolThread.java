package com.getjavajob.training.web1609.pogodaevp.lesson15.task1;


public class PoolThread extends Thread {
    private BlockingQueue taskQueue = null;
    private boolean isStopped = false;

    //"выдаем" потоку очередь задач
    public PoolThread(BlockingQueue queue) {
        taskQueue = queue;
    }

    //в теле run из очереди тасок получаем таск на исполнение и запускаем его
    public void run() {
        while (!isStopped()) {
            try {
                Runnable runnable = (Runnable) taskQueue.dequeue();
                runnable.run();
            } catch (Exception e) {
                System.out.println("Smth went wrong");
            }
        }
    }

    public synchronized void doStop() {
        isStopped = true;
        interrupt();
    }

    public synchronized boolean isStopped() {
        return isStopped;
    }
}
