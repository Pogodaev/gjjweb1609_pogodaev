package com.getjavajob.training.web1609.pogodaevp.lesson03.task1.readers;

import com.getjavajob.training.web1609.pogodaevp.lesson02.Human;
import com.getjavajob.training.web1609.pogodaevp.lesson02.OrgType;
import com.getjavajob.training.web1609.pogodaevp.lesson02.Organization;
import com.getjavajob.training.web1609.pogodaevp.lesson02.Shareholder;
import com.getjavajob.training.web1609.pogodaevp.lesson03.task1.Data;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul on 10.10.16.
 */
class XMLSAX extends DefaultHandler {
    private List<Data> datas = new ArrayList<>();
    private Data data = null;
    private String content = "";
    private String thisElem = "";
    private Human h = new Human();
    private Shareholder shareholder = new Shareholder();
    private List<Shareholder> shareholders = new ArrayList();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        thisElem = qName;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        thisElem = "";
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        content = String.copyValueOf(ch, start, length).trim();
        if (thisElem.equals("data")) {
            data = new Data();
            datas.add(data);
            data.setList(shareholders);
        }
        if (thisElem.equals("orgName")) {
            data.setOrganization(new Organization(new String(ch, start, length)));

        }
        if (thisElem.equals("orgType")) {
            data.setOrgType(OrgType.findByName(content.toLowerCase()));
        }
        if (thisElem.equals("name")) {
            h.setName(content);
        }
        if (thisElem.equals("surname")) {
            h.setLastname(content);
        }
        if (thisElem.equals("lastname")) {
            h.setSurname(content);
            data.setSeo(h);
        }
        if (thisElem.equals("capital")) {
            data.setCapital(Double.parseDouble(content));
        }
        if (thisElem.equals("parent")) {
            data.setParentOrg(content);
        }

        if (thisElem.equals("percent")) {
            shareholder.setPercent(Double.parseDouble(content));
        }
        if (thisElem.equals("name")) {
            shareholder.setHuman(new Human(content, "", ""));
            shareholders.add(shareholder);
        }
    }

    public List<Data> getDatas() {
        return datas;
    }

    public void setDatas(List<Data> datas) {
        this.datas = datas;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getThisElem() {
        return thisElem;
    }

    public void setThisElem(String thisElem) {
        this.thisElem = thisElem;
    }

    public Human getH() {
        return h;
    }

    public void setH(Human h) {
        this.h = h;
    }

    public Shareholder getShareholder() {
        return shareholder;
    }

    public void setShareholder(Shareholder shareholder) {
        this.shareholder = shareholder;
    }

    public List<Shareholder> getShareholders() {
        return shareholders;
    }

    public void setShareholders(List<Shareholder> shareholders) {
        this.shareholders = shareholders;
    }
}


