import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by paul on 06.12.16.
 */
public class ConnectDB {
    private static String JDBC_DRIVER = "";
    static String DB_URL = "";
    static String USER = "";
    static String PASS = "";

    public static String getJdbcDriver() {
        return JDBC_DRIVER;
    }

    //чет есть у меня мысль что в человеческом мире разработки это все в static блоке живет
    public static void loadProperties() throws IOException {
        Properties prop = new Properties();
        InputStream input = ConnectDB.class.getResourceAsStream("properties.properties");

        prop.load(input);

        JDBC_DRIVER = prop.getProperty("driver");
        DB_URL = prop.getProperty("url");
        USER = prop.getProperty("user");
        PASS = prop.getProperty("pass");
    }

    public Connection getConn() {
        try {
            loadProperties();
            Class.forName(getJdbcDriver());
            return DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
