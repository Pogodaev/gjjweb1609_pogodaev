package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by paul on 16.09.16.
 */
public class EmailCheck implements Check {
    public static final Pattern VALID_EMAIL =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Override
    public boolean check(Product product) throws ValidationException {
        Matcher matcher = VALID_EMAIL.matcher(product.getEmail());
        if (matcher.find()) {
            return true;
        } else {
            throw new ValidationException("Плохой адресс: " + product.getEmail());
        }
    }
}
