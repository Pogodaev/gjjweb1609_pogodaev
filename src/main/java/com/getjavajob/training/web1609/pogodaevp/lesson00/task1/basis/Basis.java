package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis;

/**
 * Created by paul on 13.09.16.
 */
public interface Basis {
    int returnCorners();

    double returnSide();

    double countBasisArea();
}
