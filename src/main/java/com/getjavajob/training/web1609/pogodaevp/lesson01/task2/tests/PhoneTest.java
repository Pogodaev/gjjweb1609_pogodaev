package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.tests;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Validator;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.Check;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.PhoneCheck;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by paul on 12.10.16.
 */
public class PhoneTest {
    private Product product;
    private Validator validator;

    @Before
    public void init() {
        System.out.println("Before");
        ArrayList<Check> list = new ArrayList<>();
        list.add(new PhoneCheck());
        this.product = new Product();
        this.validator = new Validator(list);
    }

    @Test
    public void testPhone() throws ValidationException {
        this.product.setPhone("+790000000");
        assertEquals(true, new PhoneCheck().check(this.product));
    }

    @Test(expected = ValidationException.class)
    public void testPhoneFail() throws ValidationException {
        this.product.setPhone("790000000");
        this.validator.validate(this.product);
    }
}
