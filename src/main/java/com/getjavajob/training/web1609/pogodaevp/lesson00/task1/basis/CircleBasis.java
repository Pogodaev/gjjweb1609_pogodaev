package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis;

/**
 * Created by paul on 13.09.16.
 */
public class CircleBasis extends AbstractBasis {
    private double radius;

    public CircleBasis(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double returnSide() {
        return radius;
    }

    double countCircleArea(double radius) {
        return Math.PI * radius * radius;
    }

    @Override
    public double countBasisArea() {
        return countCircleArea(getRadius());
    }

}
