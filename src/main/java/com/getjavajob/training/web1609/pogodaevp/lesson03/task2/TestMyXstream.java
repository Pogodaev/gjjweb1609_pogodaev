package com.getjavajob.training.web1609.pogodaevp.lesson03.task2;

import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static com.getjavajob.training.web1609.pogodaevp.lesson03.task2.MyXstream.fromXML;
import static com.getjavajob.training.web1609.pogodaevp.lesson03.task2.MyXstream.toXML;
import static org.junit.Assert.assertEquals;

/**
 * Created by paul on 12.10.16.
 */
public class TestMyXstream {
    private Sample sample;
    private Human human;

    @Before
    public void init() {
        this.human = new Human();
        this.human.setFamily("Popov");
        this.sample = new Sample();
        this.sample.setMyHuman(this.human);
    }

    @Test
    public void testToXml() throws NoSuchFieldException, IllegalAccessException, JAXBException {
        this.sample.setAge(15);
        this.sample.setName("Car");

        String cmp = "<com.getjavajob.training.web1609.pogodaevp.lesson03.task2.Sample>\n" +
                "<thisName>\n" +
                "    Car\n" +
                "</thisName>\n" +
                "<age age=\"id\">\n" +
                "    15\n" +
                "</age>\n" +
                "<myHuman>\n" +
                "    <Chelovek>\n" +
                "<family>\n" +
                "    Popov\n" +
                "</family>\n" +
                "</Chelovek>\n" +
                "</myHuman>\n" +
                "</com.getjavajob.training.web1609.pogodaevp.lesson03.task2.Sample>";
        String xml = toXML(this.sample);
        assertEquals(cmp, xml);
    }

    @Test
    public void testFromXML() throws IOException, NoSuchMethodException {
        String nameCmp = "Hello";
        int ageCmp = 12;
        String famCmp = "OLeg";
        String cmp = "Sample>\n" +
                "<thisName>\n" +
                "    Hello\n" +
                "</thisName>\n" +
                "<age age=\"id\">\n" +
                "    12\n" +
                "</age>\n" +
                "<myHuman>\n" +
                "    <Chelovek>\n" +
                "<family>\n" +
                "    OLeg\n" +
                "</family>\n" +
                "</Chelovek>\n" +
                "</myHuman>\n" +
                "Sample>";
        InputStream stream = new ByteArrayInputStream(cmp.getBytes(StandardCharsets.UTF_8));
        Sample s = (Sample) fromXML(stream);
        stream.close();

        assertEquals(nameCmp, s.getName());
        assertEquals(ageCmp, s.getAge());
        assertEquals(famCmp, s.getMyHuman().getFamily());
    }
}
