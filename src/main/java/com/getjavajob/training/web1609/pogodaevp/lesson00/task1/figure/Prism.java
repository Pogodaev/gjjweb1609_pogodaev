package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.figure;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis.CornerBasis;

/**
 * Created by paul on 13.09.16.
 */
public class Prism extends StrightLike implements Figure {

    public Prism(CornerBasis prismBasis, int height) {
        super(prismBasis, height);
    }

    public double getHeight() {
        return super.getHeight();
    }

    public void setHeight(double height) {
        super.setHeight(height);
    }

    @Override
    public double countSurfaceArea() {
        return getBasis().returnCorners() * getHeight() * getBasis().returnSide() + 2 * getBasis().countBasisArea();
    }

}
