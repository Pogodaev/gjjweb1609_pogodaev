package com.getjavajob.training.web1609.pogodaevp.lesson15.task3;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

/**
 * Created by paul on 05.01.17.
 */
public class MaxVal extends RecursiveAction {
    private static long max = Long.MIN_VALUE;
    private int low;
    private int high;
    private Long[] arr;
    private int threshold = 5000;

    public MaxVal(int low, int high, Long[] arr) {
        this.low = low;
        this.high = high;
        this.arr = arr;
    }

    @Override
    protected void compute() {
        if (high - low < threshold ) {
            Long[] tar = Arrays.copyOfRange(arr, low, high);
            List<Long> l = Arrays.asList(tar);
            Long tmpmax = Collections.max(l);
            max = tmpmax > max ? tmpmax : max;
        } else {
            int mid = (low + high) >>> 1;
            invokeAll(new MaxVal(low, mid - 1, arr), new MaxVal(mid, high, arr));
        }
    }

    public static void main(String[] args) {
        long[] arr = new long[5000000];
        Random random = new Random();

        Long[] newarr = new Long[arr.length];
        int j = 0;
        for (int i = 0; i < 5000000; i++) {
            arr[i] = (long) random.nextInt();
        }

        for (long temp : arr) {
            newarr[j++] = temp;
        }
        MaxVal m = new MaxVal(0, arr.length - 1, newarr);

        ForkJoinPool pool = new ForkJoinPool();
        long start = System.currentTimeMillis();
        System.out.println("Time start ");
        pool.invoke(m);
        System.out.println("Finish " + (System.currentTimeMillis() - start));
        System.out.println(max);
    }
}
