package com.getjavajob.training.web1609.pogodaevp.lesson02;

import com.getjavajob.training.web1609.pogodaevp.lesson02.IOFilesClass.LoadExit;
import com.getjavajob.training.web1609.pogodaevp.lesson02.IOFilesClass.ReadFile;
import com.getjavajob.training.web1609.pogodaevp.lesson02.exceptions.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.getjavajob.training.web1609.pogodaevp.lesson02.IOFilesClass.LoadStart.loadFromFile;
import static com.getjavajob.training.web1609.pogodaevp.lesson02.IOFilesClass.ReadFile.stringToData;

/**
 * Created by paul on 19.09.16.
 */
public class Main {
    public static void main(String[] args) throws CapitalException, OrgTypeException, ShareHolderException, StakeException, IOException, FileFormatException {
        Datas datasRead = null; //From *.dat file
        List<Data> datas = null;  //From TXT File args[0]
        List<Data> summuryData = new ArrayList<>();

        /**
         * If we have txt filename as argument we have to read it to datas List
         * */
        if (args.length != 0) {
            File f = new File(args[0]);
            if (f.exists() && !f.isDirectory()) {
                ReadFile readFile = new ReadFile();
                datas = readFile.readFromFile(args[0]);
            }
        }
        /**
         * If we have oOrganaziations.dat we have to read it to datFile List
         * then we fill List summuryData
         * */

        File f = new File("oOrganizations.dat");
        System.out.println(f.getCanonicalPath());
        if (f.exists() && !f.isDirectory()) {
            datasRead = loadFromFile("oOrganizations.dat");
            if (datas != null) {
                for (Data d : datas) {
                    String s = d.getOrganization().getOrgName();
                    for (Data m : datasRead.getDataList()) {
                        if (s.equals(m.getOrganization().getOrgName())) {
                            summuryData.add(d);
                        }
                    }
                }
            }
        }

        if (datas != null) {
            summuryData.addAll(datas);
        }
        if (datasRead != null) {
            summuryData.addAll(datasRead.getDataList());
        }

        if (datasRead != null) {
            for (Data d : datasRead.getDataList()) {
                System.out.println(d);
            }
        }

        if (summuryData != null) {
            System.out.println(getRichestGuy(summuryData));
            System.out.println(countDaughters(summuryData));
        }

        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        List<Data> listForLoad = new ArrayList<>();
        Data data;
        LoadExit le = new LoadExit();
        while (true) {
            try {

                System.out.println("Enter Organiztion:");
                String str = bf.readLine();
                if (str == "") {
                    break;
                }
                listForLoad.add(stringToData(str));


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        le.loadToFileBeforeExit(new Datas(listForLoad));
    }


    public static String getRichestGuy(List<Data> list) {
        String result = "";
        List<Shareholder> shareholders;
        HashMap<String, Double> stat = new HashMap<>();
        double cmp;

        for (Data d : list) {
            Double capital = d.getCapital();
            shareholders = d.getList();

            for (Shareholder shareholder : shareholders) {
                String name = shareholder.getHuman().getName();
                Double percent = shareholder.getPercent() * capital;

                if (stat.containsKey(name)) {
                    cmp = stat.get(name);
                } else {
                    cmp = 0.0;
                }
                stat.put(name, percent + cmp);
            }
        }

        cmp = 0.0;
        for (Map.Entry<String, Double> entry : stat.entrySet()) {
            if (cmp < entry.getValue()) {
                result = entry.getKey();
                cmp = entry.getValue();
            }
        }
        return "The richest: " + result;
    }

    public static String countDaughters(List<Data> list) {
        String str = "";
        ArrayList<String> orgs = new ArrayList<>();

        HashMap<String, Integer> stat = new HashMap<>();
        HashMap<String, String> orgPorg = new HashMap<>();

        for (Data d : list) {
            String name = d.getOrganization().getOrgName();
            String pName = d.getParentOrg();
            orgPorg.put(name, pName);
            stat.put(name, 0);
            orgs.add(name);
        }

        for (String org : orgs) {
            for (Map.Entry<String, String> entry : orgPorg.entrySet()) {
                String val = entry.getValue();
                int k = 0;
                if (org.equals(val)) {
                    k = stat.get(org);
                    stat.put(org, ++k);
                }
            }
        }

        int cmp = 0;
        for (Map.Entry<String, Integer> entry : stat.entrySet()) {
            if (cmp < entry.getValue()) {
                cmp = entry.getValue();
                str = entry.getKey();
            }
        }
        return str;
    }
}
