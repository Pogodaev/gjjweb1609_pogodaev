package com.getjavajob.training.web1609.pogodaevp.lesson15.task1;

import java.util.ArrayList;

import java.util.List;

public class ThreadPool {
    private BlockingQueue taskQueue = null;
    private List<PoolThread> threads = new ArrayList<>();
    private boolean isStopped = false;

    public ThreadPool(int countThreads, int tasksCount) {
        taskQueue = new BlockingQueue(tasksCount);

        for (int i = 0; i < countThreads; i++) {
            threads.add(new PoolThread(taskQueue));
        }

        for (PoolThread thread : threads) {
            thread.start();
        }
    }

    public synchronized void execute(Runnable task) throws Exception {
        if (isStopped) {
            throw new IllegalStateException("ThreadPool is stopped");
        }
        taskQueue.enqueue(task);
    }

    public synchronized void stop() {
        isStopped = true;
        for (PoolThread thread : threads) {
            thread.doStop();
        }
    }
}
