package com.getjavajob.training.web1609.pogodaevp.lesson02;

import java.io.Serializable;

/**
 * Created by paul on 19.09.16.
 */
public final class Organization implements Serializable {
    private String orgName;

    public Organization() {
    }

    public Organization(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "orgName='" + orgName + '\'' +
                '}';
    }
}
