package com.getjavajob.training.web1609.pogodaevp.lesson01.task1.basis;

/**
 * Created by paul on 13.09.16.
 */
public interface Basis {
    double countBasisArea();

    double countCrossSection(double crossHeight);
}
