package com.getjavajob.training.web1609.pogodaevp.lesson09.task2;

import java.io.*;
import java.util.concurrent.*;

/**
 * Created by paul on 22.10.16.
 */
public class ThreadsPool {
    private static CountDownLatch countDownLatch;
    private static String text;
    final static char[] alphabet = {'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'};
    final static int UP = alphabet.length;
    private char myChar = 0;
    static ConcurrentMap<Character, Integer> map = new ConcurrentHashMap<>();

    public static void main(String[] args) throws FileNotFoundException, InterruptedException {
        text = readFile("Text.txt").toLowerCase();
        countDownLatch = new CountDownLatch(UP);
        ExecutorService threadPool = Executors.newCachedThreadPool();
        for (char c : alphabet) {
            threadPool.execute(new CountThread(c));
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            System.out.println("Smth goes bad... \n" + e.getMessage());
        }
        threadPool.shutdown();
    }

    public static String readFile(String filePath) throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        BufferedReader bf = new BufferedReader(new FileReader(new File(filePath).getAbsolutePath()));

        try {
            while (bf.ready()) {
                sb.append(bf.readLine());
            }
            bf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = sb.toString();
        return res;
    }

    private static class CountThread extends Thread {
        private char ch;
        private int cnt;

        public CountThread(char ch) {
            this.ch = ch;
        }

        @Override
        public void run() {
            for (Character c : text.toLowerCase().toCharArray()) {
                if (c == ch) {
                    cnt++;
                }
            }
            if (cnt > 0) {
                map.put(ch, cnt);
            }
            countDownLatch.countDown();
        }
    }
}
