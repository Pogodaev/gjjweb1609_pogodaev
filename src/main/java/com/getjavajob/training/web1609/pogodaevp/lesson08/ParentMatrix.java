package com.getjavajob.training.web1609.pogodaevp.lesson08;

import java.io.*;

/**
 * Created by paul on 26.10.16.
 */
public class ParentMatrix {
    public static void mult(int row, int col, int a[][], int b[][], int c[][]) throws InterruptedException {
        for (int k = 0; k < b.length; k++) {
            c[row][col] += a[row][k] * b[k][col];
        }
    }

    public static int[][] readFromFile(String filename) throws IOException {
        File file = new File(filename);
        FileReader fr = new FileReader(file);

        char array[] = new char[(int) file.length()];
        fr.read(array, 0, (int) file.length());

        String[] tmp = (new String(array)).split("\n");
        int ints[][] = new int[tmp.length][];

        for (int i = 0; i < tmp.length; i++) {
            String[] elems = tmp[i].split(" ");
            ints[i] = new int[elems.length];
            for (int j = 0; j < elems.length; j++) {
                ints[i][j] = Integer.parseInt(elems[j]);
            }
        }
        return ints;
    }

    public static void writeToFile(int[][] matrix) throws FileNotFoundException, UnsupportedEncodingException {
        System.out.println("Let's write your result into the file:");
        PrintWriter writer = new PrintWriter("result.txt", "UTF-8");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[j].length; j++) {
                writer.print(matrix[i][j] + " ");
            }
            writer.println();
        }
        writer.close();
    }

    public static void printMatrix(int[][] c) {
        for (int i = 0; i < c.length; i++) {
            for (int j = 0; j < c[i].length; j++) {
                System.out.print(c[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
