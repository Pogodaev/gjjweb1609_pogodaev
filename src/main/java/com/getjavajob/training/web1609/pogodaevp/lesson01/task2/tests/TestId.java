package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.tests;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Validator;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.Check;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.IdCheck;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by paul on 12.10.16.
 */
public class TestId {
    private Product product;
    private Validator validator;

    @Before
    public void init() {
        System.out.println("Before");
        ArrayList<Check> list = new ArrayList<>();
        list.add(new IdCheck());
        this.product = new Product();
        this.validator = new Validator(list);
    }

    @Test
    public void testId() throws ValidationException {
        this.product.setClientId("MYID");
        assertEquals(true, new IdCheck().check(this.product));
    }

    @Test(expected = ValidationException.class)
    public void testIdFalse() throws ValidationException {
        this.product.setClientId("mYID");
        this.validator.validate(this.product);
    }

}
