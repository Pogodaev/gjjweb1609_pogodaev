package com.getjavajob.training.web1609.pogodaevp.lesson00.task2.checkers;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task2.Product;

/**
 * Created by paul on 16.09.16.
 */
public class IdCheck implements Check {
    @Override
    public boolean check(Product p) {
        if (p.getClientId() != null && p.getClientId() != "" && p.getClientId().equals(p.getClientId().toUpperCase())) {
            return true;
        }
        return false;
    }
}
