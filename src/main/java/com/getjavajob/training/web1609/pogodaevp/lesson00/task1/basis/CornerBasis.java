package com.getjavajob.training.web1609.pogodaevp.lesson00.task1.basis;

/**
 * Created by paul on 13.09.16.
 */
public class CornerBasis extends AbstractBasis {
    private int corners;
    private double side;

    public CornerBasis(int corners, double side) {
        this.corners = corners;
        this.side = side;
    }

    public int getCorners() {
        return corners;
    }

    public void setCorners(int corners) {
        this.corners = corners;
    }

    public double getBorder() {
        return side;
    }

    double countCornerArea(int corners, double border) {
        return (corners * border * border) / (4 * Math.tan(Math.PI / corners));
    }

    @Override
    public double countBasisArea() {
        return countCornerArea(getCorners(), getBorder());
    }

    @Override
    public int returnCorners() {
        return corners;
    }

    @Override
    public double returnSide() {
        return side;
    }
}
