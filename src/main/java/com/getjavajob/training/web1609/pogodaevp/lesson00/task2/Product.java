package com.getjavajob.training.web1609.pogodaevp.lesson00.task2;

import java.util.Date;

/**
 * Created by paul on 14.09.16.
 */
public class Product {
    private String pName;
    private int needQuantity;
    private int stockQuantity;
    private int price;
    private int discount;
    private String clientId;
    private String email;
    private String phone;
    private Date date;

    public Product() {
    }

    public Product(String pName, int needQuantity, int stockQuantity, int price, int discount, String clientId, String email, String phone, Date date) {
        this.pName = pName;
        this.needQuantity = needQuantity;
        this.stockQuantity = stockQuantity;
        this.price = price;
        this.discount = discount;
        this.clientId = clientId;
        this.email = email;
        this.phone = phone;
        this.date = date;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public void setNeedQuantity(int needQuantity) {
        this.needQuantity = needQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getpName() {
        return pName;
    }

    public int getNeedQuantity() {
        return needQuantity;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public int getPrice() {
        return price;
    }

    public int getDiscount() {
        return discount;
    }

    public String getClientId() {
        return clientId;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public Date getDate() {
        return date;
    }
}
