package com.getjavajob.training.web1609.pogodaevp.lesson10;

import com.getjavajob.training.web1609.pogodaevp.lesson07.task2.MultiSort;
import com.getjavajob.training.web1609.pogodaevp.lesson07.task2.Thing;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

/**
 * Created by paul on 22.10.16.
 */
public class Runner {
    private static SingleQuickSort sqs = new SingleQuickSort();
    private static MultiSort mqs = new MultiSort();

    public static void main(String[] args) {
        Random random = new Random();
        Thing[] m;
        Thing[] cp = new Thing[500_000];
        Integer[] c = new Integer[500_000];
        m = cp;

        for (int i = 0; i < 500000; i++) {
            Thing to = new Thing(String.valueOf(random.nextInt()));
            cp[i] = to;
            c[i] = random.nextInt();
        }

        ForkJoinPool pool = new ForkJoinPool();
        System.out.println("Pool is created for " + pool.getParallelism() + " cores"); //2 :(((
        ForkJoinSort forkJoinSort = new ForkJoinSort<Integer>(c);
        long timeBefore = System.currentTimeMillis();
        pool.invoke(forkJoinSort);
        System.out.println("ForkJoinSort " + (System.currentTimeMillis() - timeBefore));

        timeBefore = System.currentTimeMillis();
        sqs.sort(cp);
        System.out.println("SingleQuickSort " + (System.currentTimeMillis() - timeBefore));

        timeBefore = System.currentTimeMillis();
        mqs.quicksort(m, 0, m.length - 1, 10);
        System.out.println("MultiQuickSort " + (System.currentTimeMillis() - timeBefore));

        for (Integer s : c) {
            System.out.println(s);
        }
    }
}
