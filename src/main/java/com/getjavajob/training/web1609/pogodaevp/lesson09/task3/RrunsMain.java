package com.getjavajob.training.web1609.pogodaevp.lesson09.task3;

import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * Created by paul on 22.10.16.
 */
public class RrunsMain {
    private static Semaphore generatorSem = new Semaphore(1);
    private static Semaphore evenSem = new Semaphore(0);
    private static Semaphore oddSem = new Semaphore(0);
    static int SHARE;

    public static void main(String[] args) throws InterruptedException {
        new Generator().start();
        new Thread(new Consumer(evenSem), "EvenConsumer").start();
        new Thread(new Consumer(oddSem), "OddConsumer").start();
    }

    public static class Consumer extends Thread {
        private Semaphore semaphore;

        public Consumer(Semaphore semaphore) {
            this.semaphore = semaphore;
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + " потребил " + SHARE);
                    generatorSem.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class Generator extends Thread {
        Random r = new Random();

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    generatorSem.acquire();
                    SHARE = r.nextInt();
                    System.out.println("Генерирую число... " + SHARE);
                    if (SHARE % 2 == 1) {
                        evenSem.release();
                    } else {
                        oddSem.release();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
