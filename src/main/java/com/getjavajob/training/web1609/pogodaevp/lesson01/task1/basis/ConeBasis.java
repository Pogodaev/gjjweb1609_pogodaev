package com.getjavajob.training.web1609.pogodaevp.lesson01.task1.basis;

/**
 * Created by paul on 13.09.16.
 */
public class ConeBasis extends AbstractBasis implements Cloneable {
    private double height;
    private double radius;

    @Override
    public ConeBasis clone() {
        try {
            return (ConeBasis) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
//        return (ConeBasis) super.clone();
    }

    public ConeBasis(double height, double radius) {
        this.radius = radius;
        this.height = height;
    }

    public double getRadius() {
        return radius;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public double countBasisArea() {
        return countCircleArea(getRadius());
    }

    @Override
    public double countCrossSection(double crossHeight) {
        double d = getHeight();
        return (countBasisArea() * crossHeight * crossHeight) / (d * d);
    }
}
