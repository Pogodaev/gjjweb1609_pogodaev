package com.getjavajob.training.web1609.pogodaevp.lesson10;


/**
 * Created by paul on 05.11.16.
 */
public class SingleQuickSort<T extends Comparable<T>> {

    public void sort(T[] t) {
        sort(t, 0, t.length - 1);
    }

    public void sort(T[] t, int threshold) {
        sort(t);
    }

    protected void sort(T[] array, int left, int right) {
        if (left < right) {
            int pivot = partition(array, left, right);
            sort(array, left, pivot);
            sort(array, pivot + 1, right);
        }
    }

    protected int partition(T[] t, int left, int right) {
        T pivot = t[left + (right - left) / 2];
        int lt = left - 1;
        int rt = right + 1;
        while (true) {
            do {
                lt++;
            }
            while (t[lt].compareTo(pivot) < 0);
            do {
                rt--;
            }
            while (t[rt].compareTo(pivot) > 0);
            if (lt >= rt) {
                return rt;
            }
            swap(t, lt, rt);
        }
    }

    private void swap(T[] t, int x, int y) {
        T temp = t[x];
        t[x] = t[y];
        t[y] = temp;
    }
}
