package com.getjavajob.training.web1609.pogodaevp.lesson00.task2;

import com.getjavajob.training.web1609.pogodaevp.lesson00.task2.checkers.Check;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul on 15.09.16.
 */
public class Validator {
    private List<Check> checkList;

    public Validator(ArrayList<Check> checkList) {
        this.checkList = checkList;
    }

    public boolean validate(Product product) {
        for (Check ch : checkList) {
            if (!ch.check(product)) {
                System.out.println(ch.getClass().getSimpleName());
                return false;
            }
        }
        return true;
    }
}
