package com.getjavajob.training.web1609.pogodaevp.lesson01.task2.tests;

import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Product;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.ValidationException;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.Validator;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.Check;
import com.getjavajob.training.web1609.pogodaevp.lesson01.task2.checkers.PriceCheck;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by paul on 12.10.16.
 */
public class PriceTest {
    private Product product;
    private Validator validator;

    @Before
    public void init() {
        System.out.println("Before");
        ArrayList<Check> list = new ArrayList<>();
        list.add(new PriceCheck());
        this.product = new Product();
        this.validator = new Validator(list);
    }

    @Test
    public void testPrice() throws ValidationException {
        this.product.setPrice(1);
        assertEquals(true, new PriceCheck().check(this.product));
    }

    @Test(expected = ValidationException.class)
    public void testPriceFail() throws ValidationException {
        this.product.setPrice(-1);
        this.validator.validate(this.product);
    }
}
