import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by paul on 08.12.16.
 */
public class SqlClient {
    private final ConnectDB connectDB = new ConnectDB();
    static Connection connection = null;
    static Statement stmt = null;

    public SqlClient() {
        System.out.println("Connection to database...");
        connection = connectDB.getConn();
    }

    public String execute(String sql) {
        String result = "";
        ResultSet rs = null;
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int id  = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                result += "id " + id + " name " + name + " age " + age;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }
}
